/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;



/**
 *
 * @author joao
 */
public class Card4 extends EventCard{
    
        public Card4(Datagame g) {
        super(g);
    }
    
     @Override
    public void Dia1(){
        this.setEventName("Death of Leader");
        this.getDatagame().setActionPointsAtual(2);
        this.getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        if(getDatagame().isTwoOnZero())
                    return;
        
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
         this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-1);
          if(getDatagame().getEnemy().getTrackRight()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
          }
        }
        
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        
    }
    
    @Override
    public void Dia2(){
        this.setEventName("Gate Fortified");
        this.getDatagame().setActionPointsAtual(2);
        this.getDatagame().setDrmBatteringRam(1);
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
         this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
         if(getDatagame().getEnemy().getTrackLeft()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
         }
        }
        
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
    }
    
    @Override
    public void Dia3(){
        this.getDatagame().setActionPointsAtual(3);
        this.getDatagame().setDrmSiegeTower(1);
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
            this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-1);
             if(getDatagame().getEnemy().getTrackRight()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        this.setEventName("Flaming Arrows");
    }
}
