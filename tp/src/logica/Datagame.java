/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import logica.cards.Card1;
import logica.cards.Card2;
import logica.cards.Card3;
import logica.cards.Card4;
import logica.cards.Card5;
import logica.cards.Card6;
import logica.cards.Card7;
import logica.cards.EventCard;

/**
 *
 * @author joao
 */
public class Datagame implements Serializable{
    private int diaAtual;
    private int actionPointsAtual;
    
    private Player jogador;
    private Enemy enemy;
    private Dice dado;
    private int Gameover;
    private boolean addActionPointsUsed;
    private boolean boilingUsed;
    private boolean freeMovementUsed;
    private boolean siegeTowerRemoved;
    private boolean blockActions;
    
    private int drmMorale;
    private int drmSabotage;
    private int drmCoupure;
    private int drmBatteringRam;
    private int drmLadder;
    private int drmSiegeTower;
    private int drmRaid;
    private int drmCloseCombat;
    
    private final List<String> msgLog;
     
    private List<EventCard>baralho;
    private List<EventCard>Removidas;
    private List<EventCard> copiaReset;

    public Datagame() {
        
        baralho= new ArrayList<>();   
        Removidas=new ArrayList<>();
        copiaReset=new ArrayList<>();
        this.baralho.add(new Card1(this));
        this.baralho.add(new Card2(this));
        this.baralho.add(new Card3(this));
        this.baralho.add(new Card4(this));
        this.baralho.add(new Card5(this));
        this.baralho.add(new Card6(this));
        this.baralho.add(new Card7(this));
        dado=new Dice();
        enemy=new Enemy();
        jogador=new Player();
        this.blockActions=false;
        this.siegeTowerRemoved=false;
        Collections.shuffle(baralho);
        this.diaAtual=1;
        Gameover=0;
        this.freeMovementUsed=false;
        copiaReset.addAll(baralho);
        msgLog = new ArrayList<>();
    }    
    
    public void clearMsgLog()
    {
        msgLog.clear();
    }
    
    public void addMsgLog(String msg)
    {
        msgLog.add(msg);
    }
    public void erro(){
        
        this.addMsgLog("NAO PODE EXECUTAR ESTA ACCAO\n");
    }
           
    
    public List<String> getMsgLog()
    {
        return msgLog;
    }

    public void CloseCombatArea(){
        if(NoOneClosecombatArea()){
            this.erro();
            return;
        }
        dado.roll();
        int n = dado.getNumero();
        n+=this.getDrmCloseCombat();
        this.addMsgLog("Dado ja com drm close combat:"+n+"\n");
        int []array=getEnemyInCloseArea();
        
        if(n==1)
            Gameover=1;
        if(n>4){
            enemy.setTrackLeft(getEnemy().getTrackLeft()+ array[0]);
            enemy.setTrackCenter(getEnemy().getTrackCenter()+array[1]);
            enemy.setTrackRight(getEnemy().getTrackRight()+array[2]);
          
        }
    }
    public boolean isMandatory(){
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
       return left==0 && center==0 || left==0 && right==0 || center==0&& right==0;
    }
    public int[] getEnemyInCloseArea(){
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
        int array[]=new int[3];
        if(left==0)
            array[0]=1;
        if(center==0)
            array[1]=1;
        if(right==0)
            array[2]=1;
        return array;       
    }
    public boolean allCloseCombatArea(){
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
        return left==0 && center==0 && right==0;
    }
    
    public boolean NoOneClosecombatArea(){
        
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
        return left!=0 && center!=0 && right!=0;
    }
    
    public boolean NoOneCloseCircleSpaces(){
        
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
        return left!=1 && center!=1 && right!=1;
    }
    
    public int[] getEnemyInCircleSpaces(){
        int left,center,right;
        left=getEnemy().getTrackLeft();
        center=getEnemy().getTrackCenter();
        right=getEnemy().getTrackRight();
        int array[]=new int[3];
        if(left==1)
            array[0]=1;
        if(center==1)
            array[1]=1;
        if(right==1)
            array[2]=1;
        return array;       
    }
    
    public boolean isTwoOnZero(){
        return jogador.getMorale()==0 && jogador.getSupplies()==0 || jogador.getMorale()==0 && jogador.getWallStrength()==0 || jogador.getWallStrength()==0 && jogador.getSupplies()==0;
    }

    public int getGameover() {
        return Gameover;
    }
    
    public Dice getDado() {
        return dado;
    }

    public void setDado(Dice dado) {
        this.dado = dado;
    }
    
    public List<EventCard> GetRemovidasBaralho(){
        return Removidas;
    }

    public List<EventCard> getBaralho() {
        return baralho;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }
   
    public int getDiaAtual() {
        return diaAtual;
    }

    public void setDiaAtual(int diaAtual) {
        this.diaAtual = diaAtual;
    }

    public int getActionPointsAtual() {
        return actionPointsAtual;
    }

    public void setActionPointsAtual(int actionPointsAtual) {
        this.actionPointsAtual = actionPointsAtual;
    }

    public Player getJogador() {
        return jogador;
    }

    public void setJogador(Player jogador) {
        this.jogador = jogador;
    }

    public int getDrmMorale() {
        return drmMorale;
    }

    public void setDrmMorale(int drmMorale) {
        this.drmMorale = drmMorale;
    }

    public int getDrmSabotage() {
        return drmSabotage;
    }

    public void setDrmSabotage(int drmSabotage) {
        this.drmSabotage = drmSabotage;
    }

    public int getDrmCoupure() {
        return drmCoupure;
    }

    public void setDrmCoupure(int drmCoupure) {
        this.drmCoupure = drmCoupure;
    }

    public int getDrmBatteringRam() {
        return drmBatteringRam;
    }

    public void setDrmBatteringRam(int drmBatteringRam) {
        this.drmBatteringRam = drmBatteringRam;
    }

    public int getDrmLadder() {
        return drmLadder;
    }

    public void setDrmLadder(int drmLadder) {
        this.drmLadder = drmLadder;
    }

    public int getDrmSiegeTower() {
        return drmSiegeTower;
    }

    public void setDrmSiegeTower(int drmSiegeTower) {
        this.drmSiegeTower = drmSiegeTower;
    }

    public int getDrmRaid() {
        return drmRaid;
    }

    public void setDrmRaid(int drmRaid) {
        this.drmRaid = drmRaid;
    }

    public int getDrmCloseCombat() {
        return drmCloseCombat;
    }

    public void setDrmCloseCombat(int drmCloseCombat) {
        this.drmCloseCombat = drmCloseCombat;
    }

    public boolean isAddActionPointsUsed() {
        return addActionPointsUsed;
    }

    public void setAddActionPointsUsed(boolean addActionPointsUsed) {
        this.addActionPointsUsed = addActionPointsUsed;
    }

    public boolean isBoilingUsed() {
        return boilingUsed;
    }

    public void setBoilingUsed(boolean boilingUsed) {
        this.boilingUsed = boilingUsed;
    }

    public boolean isFreeMovementUsed() {
        return freeMovementUsed;
    }

    public void setFreeMovementUsed(boolean freeMovementUsed) {
        this.freeMovementUsed = freeMovementUsed;
    }

    public boolean isSiegeTowerRemoved() {
        return siegeTowerRemoved;
    }

    public void setSiegeTowerRemoved(boolean siegeTowerRemoved) {
        this.siegeTowerRemoved = siegeTowerRemoved;
    }

    public boolean isBlockActions() {
        return blockActions;
    }

    public void setBlockActions(boolean blockActions) {
        this.blockActions = blockActions;
    }

    public void setBaralho(List<EventCard> baralho) {
        this.baralho = baralho;
    }
    
  
    public List<EventCard> getRemovidas() {
        return Removidas;
    }

    public List<EventCard> getCopiaReset() {
        return copiaReset;
    }
    
    public void resetDrm(){
        drmBatteringRam=0;
        drmCloseCombat=0;
        drmCoupure=0;
        drmLadder=0;
        drmMorale=0;
        drmRaid=0;
        drmSabotage=0;
        drmSiegeTower=0;
    }

    public void setGameover(int Gameover) {
        this.Gameover = Gameover;
    }
    

    @Override
    public String toString() {
        return "Carta"+Removidas.get(Removidas.size()-1).toString()+"\nDatagame{" + "diaAtual=" + diaAtual + ", actionPointsAtual=" + actionPointsAtual + '}'+"\n"+enemy.toString()+"\n"+jogador.toString()+"\n";
    }
    
   
    
}
