/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitTrackBoiling extends EstadoAdapter{

    public AwaitTrackBoiling(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado applyBoilingWaterAttack(int choice){
        
        if(choice==1 && getDatagame().getEnemy().getTrackLeft()!=1)
         {getDatagame().erro();return new  AwaitActionSelection(getDatagame());}
        if(choice==2 && getDatagame().getEnemy().getTrackCenter()!=1)
         {getDatagame().erro();return new  AwaitActionSelection(getDatagame());}
        if(choice==3 && getDatagame().getEnemy().getTrackRight()!=1)
         {getDatagame().erro();return new  AwaitActionSelection(getDatagame());}
        
        getDatagame().getDado().roll();
        int dice=getDatagame().getDado().getNumero();
        int resultado=0;
         
        getDatagame().addMsgLog("Dado:"+getDatagame().getDado().getNumero());
         if(choice==1){
             getDatagame().addMsgLog("DRM Ladder:"+getDatagame().getDrmLadder()+"\n");
             resultado=dice+getDatagame().getDrmLadder();
            if(resultado+1>getDatagame().getEnemy().getStrengthLeft()){
                getDatagame().getEnemy().setTrackLeft(getDatagame().getEnemy().getTrackLeft()+1);
            }
         }
         if(choice==2){
             getDatagame().addMsgLog("DRM Battering Ram:"+getDatagame().getDrmBatteringRam()+"\n");
             resultado=dice+getDatagame().getDrmBatteringRam();
            if(resultado+1>getDatagame().getEnemy().getStrengthCenter()){
                getDatagame().getEnemy().setTrackCenter(getDatagame().getEnemy().getTrackCenter()+1);
            }
         }
         if(choice==3){
            getDatagame().addMsgLog("DRM Siege Tower:"+getDatagame().getDrmSiegeTower()+"\n");
            resultado=dice+getDatagame().getDrmSiegeTower();
            if(resultado+1>getDatagame().getEnemy().getStrengthRight()) {
                getDatagame().getEnemy().setTrackRight(getDatagame().getEnemy().getTrackRight()+1);
            }   
         }
         if(resultado==1){
             getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
              if(getDatagame().isTwoOnZero())
                  return new GameOver(getDatagame());
         }
         getDatagame().setBoilingUsed(true);
         getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
         return new AwaitActionSelection(getDatagame()); 
     }
    
}
