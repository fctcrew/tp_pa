/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import logica.ObservableGame;

/**
 *
 * @author joao
 */
public class LogPanel extends JPanel implements Constants, Observer{
    private ObservableGame observableGame;
    
    private JLabel lbTitleLogs, lbLogs;
    private JTextArea area;
    
    public LogPanel(ObservableGame g)
    {
        this.observableGame = g;
        
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        
        setupComponents();
        setupLayout();
    }
    
    private void setupComponents()
    { 
        lbTitleLogs = new JLabel("Logs");
        Common.setFont(lbTitleLogs, MEDIUM_SIZE);
        
        lbLogs = new JLabel("Nothing");
        Common.setFont(lbLogs,MEDIUM_SIZE);
        //lbLogs.
        
        area = new JTextArea(4,10);
        area.setFont(new Font("Serif", Font.ITALIC, 18));
        //area.setFont();
        
    }
     
    private void setupLayout()
    {
        Box logContainer = Box.createVerticalBox();
        logContainer.add(Box.createRigidArea(new Dimension(100, 50)));
        setLayout(new BorderLayout());
            
        logContainer.add(lbTitleLogs);
//        logContainer.add(lbLogs);
        logContainer.add(new JScrollPane(area));
       
        add(logContainer, BorderLayout.CENTER);
    }
    
    @Override
    public void update(Observable o, Object arg)
    {
//        if(observableGame.getLogs().size()>1)
//            observableGame.getLogs().remove(observableGame.getLogs().size()-2);
    String s = "";
    for(int i=0;i<observableGame.getLogs().size();i++){
        s += observableGame.getLogs().get(i)+"\n";
//    lbLogs.setText(observableGame.getLogs().get(i)+"\n");
    }
//       lbLogs.setText(s);
       area.setText(s);
  
        
    }
}
