/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitGiveExtraSupplies;

/**
 *
 * @author luis_
 */
public class AwaitGiveExtraSuppliesPanel extends JPanel implements Observer{
    private ObservableGame observableGame;
    private JButton yesBtn,noBtn;
   

    public AwaitGiveExtraSuppliesPanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        yesBtn=new JButton("YES");
        noBtn=new JButton("NO");
        setupComponents();
        add(yesBtn);
        add(noBtn);
        setupLayout();
        yesBtn.addActionListener(new applyMakeYESListener());
        noBtn.addActionListener(new applyMakeNOListener());
        update(observableGame,null);
    }
    
    private void setupComponents(){
        yesBtn.setPreferredSize(new Dimension(200,30));
        yesBtn.setMinimumSize(new Dimension(200, 30));
        yesBtn.setMaximumSize(new Dimension(200, 30));
        yesBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        noBtn.setPreferredSize(new Dimension(200,30));
        noBtn.setMinimumSize(new Dimension(200, 30));
        noBtn.setMaximumSize(new Dimension(200, 30));
        noBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    private void setupLayout(){
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainer = Box.createHorizontalBox();

        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        JLabel question =new JLabel("Give extra supplies to obtain +1 DRM ?");   
        question.setAlignmentX(Component.CENTER_ALIGNMENT);
        question.setFont(new Font("Arial", Font.BOLD, 18));
        buttonsContainerCenter.add(question);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(yesBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(noBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
     
        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
                
        add(buttonsContainer, BorderLayout.CENTER);
    }


    @Override
    public void update(Observable o, Object arg) {
        setVisible(observableGame.getEstado() instanceof AwaitGiveExtraSupplies);
    }
    
    private class applyMakeYESListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.makeSpeech(true);
        }        
    }
    private class applyMakeNOListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.makeSpeech(false);
        }        
    } 
}
