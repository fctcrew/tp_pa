/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;



/**
 *
 * @author joao
 */
public class Card2 extends EventCard{
    
        public Card2(Datagame g) {
        super(g);
    }
    
       @Override
    public void Dia1(){
        this.setEventName("Ilness");
        this.getDatagame().setActionPointsAtual(2);
        
        this.getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        if(getDatagame().isTwoOnZero())
            return;
        
        this.getDatagame().getJogador().setSupplies(getDatagame().getJogador().getSupplies()-1);
        if(getDatagame().isTwoOnZero())
            return;
        
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
            
            this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-1);
             if(getDatagame().getEnemy().getTrackRight()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        

    }
    
    @Override
    public void Dia2(){
        this.setEventName("Guards Distracted");
        this.getDatagame().setActionPointsAtual(2);
        this.getDatagame().setDrmMorale(1);
        this.getDatagame().setDrmSabotage(1);
        
        int []array=this.getDatagame().getEnemy().getSlowest();
        
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
            this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-array[2]);
             if(getDatagame().getEnemy().getTrackRight()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
             }
        }
        
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-array[1]);
            if(getDatagame().getEnemy().getTrackCenter()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
            }
        }
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-array[0]);
             if(getDatagame().getEnemy().getTrackLeft()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        
        



    }
    
    @Override
    public void Dia3(){
        this.getDatagame().setActionPointsAtual(1);
        if(this.getDatagame().getEnemy().getTrebuchets()==3)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-2);
        if(this.getDatagame().getEnemy().getTrebuchets()==2)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        if(this.getDatagame().getEnemy().getTrebuchets()==1)
        {
           getDatagame().getDado().roll();
           getDatagame().addMsgLog("Trebuchet 1 foi preciso rolar dado \nDado:"+getDatagame().getDado().getNumero()+"\n");
           if(getDatagame().getDado().getNumero()>3)
               this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        }
        this.setEventName("Trebuchet Attack!");

    }
}
