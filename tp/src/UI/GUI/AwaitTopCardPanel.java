/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitTopCard;

/**
 *
 * @author luis_
 */
public class AwaitTopCardPanel extends JPanel implements Observer{
    private ObservableGame observableGame;
    
    private JButton virarCartaBtn;

    public AwaitTopCardPanel(ObservableGame observableGame) {
        
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        setVisible(observableGame.getEstado() instanceof AwaitTopCard);
        
        setOpaque(false);
        virarCartaBtn=new JButton("Tirar carta do baralho");
        add(virarCartaBtn);
        
        virarCartaBtn.setPreferredSize(new Dimension(200, 50));
        virarCartaBtn.setMinimumSize(new Dimension(200, 50));
        virarCartaBtn.setMaximumSize(new Dimension(200, 50));
        setupLayout();
        virarCartaBtn.addActionListener(new DrawTopCardListener());
        update(observableGame,null);
    }

    private void setupLayout(){
       Box buttonsContainerCenter = Box.createVerticalBox();
       Box buttonsContainer = Box.createHorizontalBox();
        
        
        
        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        buttonsContainerCenter.add(virarCartaBtn);
       
        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
                
        
        //add(lbTitle, BorderLayout.NORTH);        
        add(buttonsContainer, BorderLayout.CENTER);
   }
    
    @Override
    public void update(Observable o, Object arg) {
        setVisible(observableGame.getEstado() instanceof AwaitTopCard);
    }
    
    class DrawTopCardListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.drawTopCard();
        }        
    }
    
}
