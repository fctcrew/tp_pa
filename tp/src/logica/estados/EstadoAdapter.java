/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import java.io.Serializable;
import logica.Datagame;

/**
 *
 * @author joao
 */
public abstract class EstadoAdapter implements IEstado,Serializable{
    private Datagame datagame;
    
    public EstadoAdapter(Datagame data){
        this.datagame=data;
    }

    public Datagame getDatagame() {
        return datagame;
    }

    public void setDatagame(Datagame datagame) {
        this.datagame = datagame;
    }
    @Override
    public IEstado comecarJogo(){
        return this;
    }
    
    @Override
    public IEstado sabotage(){
        return this;
    }
    
    @Override
    public IEstado coupure(){
        return this;
    }
    
    @Override
    public IEstado supplyRaid(){
        return this;
    }
    
    @Override
    public IEstado drawTopCard(){
        return this;
    }
    
    @Override
    public IEstado closeCombatAttack(){
        return this;
    }
   @Override
    public IEstado rallyTrops(){
        return this;
    }
     @Override
    public IEstado makeSpeech(boolean choice){
            return this;
        }
    
    @Override
    public IEstado additionalActionPoints(){
            return this;
        }
    
    @Override
    public IEstado spendSuppliesOrMorale(int choice){
        return this;
    }
    @Override
    public IEstado applyArchersAttack(int value){
        return this;
    }
    
    @Override
    public IEstado archersAttack(){
        return this;
    }
    
    @Override
    public IEstado boilingWaterAttack(){
        return this;
    }
    
    @Override
    public IEstado applyBoilingWaterAttack(int choice){
        return this;
    }
    
    @Override
    public IEstado tunnelMovement(){
        return this;
    }
    
    @Override
    public IEstado applyTunnelMovement(int choice,int move){
        return this;
    }
    
    @Override
    public IEstado endOfActionSelection(){
        return this;
    }
    
    @Override
    public IEstado resetJogo(){
        return this;
    }

    
}
