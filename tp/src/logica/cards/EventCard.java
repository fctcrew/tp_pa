/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;


import java.io.Serializable;
import logica.Datagame;


/**
 *
 * @author joao
 */
public abstract class EventCard implements Serializable{
    
    private Datagame datagame;
    private String eventName;
    
    public EventCard(Datagame datagame) {
        this.datagame = datagame;
    }
  
    
    public abstract void Dia1();
    public abstract void Dia2();
    public abstract void Dia3();

    public Datagame getDatagame() {
        return datagame;
    }

    public void setJogo(Datagame datagame) {
        this.datagame= datagame;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public String toString() {
        return "EventCard{" + "eventName=" + eventName + '}';
    }
    

}
