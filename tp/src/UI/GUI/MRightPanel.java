/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import logica.ObservableGame;
import logica.estados.AwaitBeginning;

/**
 *
 * @author joao
 */
public class MRightPanel extends JPanel implements Constants, Observer{
    private ObservableGame observableGame;
    
    private PlayerCardPanel playerCardPanel;
    private JLabel diaAtual,actionPoints;
    
    public MRightPanel(ObservableGame g)
    {
        observableGame=g;
        
        observableGame.addObserver(this);
        
        setOpaque(false);
        
        setupComponents();
        setupLayout();
    }
    
    private void setupComponents()
    {       
        playerCardPanel = new PlayerCardPanel(observableGame);
        
        
       playerCardPanel.setVisible(!(observableGame.getEstado() instanceof AwaitBeginning));
//        
//        dungeonCardPanel.setVisible(!(game.getState() instanceof AwaitBeginning));
    }

    private void setupLayout()
    {
        setLayout(new BorderLayout());
        
        Box container = Box.createVerticalBox();
        
        container.add(Box.createRigidArea(new Dimension(550, 100)));
        container.add(playerCardPanel);
        
        add(container, BorderLayout.NORTH);
        
        
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainer = Box.createHorizontalBox();
        
        diaAtual =new JLabel("");   
        diaAtual.setAlignmentX(Component.CENTER_ALIGNMENT);
        diaAtual.setFont(new Font("Arial", Font.BOLD, 18)); 
        
        actionPoints =new JLabel("");   
        actionPoints.setAlignmentX(Component.CENTER_ALIGNMENT);
        actionPoints.setFont(new Font("Arial", Font.BOLD, 18)); 
      
        buttonsContainer.add(Box.createHorizontalGlue());
        buttonsContainerCenter.add(diaAtual);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(actionPoints);
        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
        add(buttonsContainer, BorderLayout.SOUTH);
//        add(diaAtual,BorderLayout.SOUTH);
//        
//        add(actionPoints,BorderLayout.SOUTH);
   
       
    }
    

    @Override
    public void update(Observable o, Object o1) {
        playerCardPanel.setVisible(!(observableGame.getEstado() instanceof AwaitBeginning));
        actionPoints.setText("Actions Points: "+observableGame.getDataGame().getActionPointsAtual());
        diaAtual.setText("Dia Atual: "+observableGame.getDataGame().getDiaAtual());
        
    }
}
