/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.Texto;

import java.io.IOException;
import java.util.Scanner;
import logica.FicheirosDeObjectos;
import logica.Jogo;
import logica.estados.AwaitActionSelection;
import logica.estados.AwaitBeginning;
import logica.estados.AwaitGiveExtraSupplies;
import logica.estados.AwaitMovementTunnel;
import logica.estados.AwaitSpendSuppliesOrMorale;
import logica.estados.AwaitTopCard;
import logica.estados.AwaitTrackArchers;
import logica.estados.AwaitTrackBoiling;
import logica.estados.GameOver;
import logica.estados.GameWin;

/**
 *
 * @author luis_
 */
public class UITexto {
    private Scanner s;
    private Jogo j;
    private boolean sair =false;
 
    public UITexto(Jogo jogo) {
        j=jogo;
        this.s = new Scanner(System.in);
    }
    private void getUserInputWhileAwaitingBeginning()
    {
        int value;
        
        System.out.println("1-Begin");
        System.out.println("2-Ler de Ficheiro");

        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
            
        value=s.nextInt();

        if(value==1)
            j.comecarJogo();
        if(value==2)
            ler();
    }
    private void getUserAwaitTopCard(){
        int value;
        
        System.out.println("1-Tirar carta do baralho");
        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
            
        value=s.nextInt();

        if(value==1)
            j.drawTopCard();
    }
    private void getUserInputAwaitActionSelection()
    {
        int value;
        
        System.out.println(j.toString());
        System.out.println("1-Archers Attack");
        System.out.println("2-Boiling Water Attack");
        System.out.println("3-Close Combat Attack");
        System.out.println("4- Coupure");
        System.out.println("5-Rally Troops");
        System.out.println("6-Tunnel Movement");
        System.out.println("7-Supply Raid");
        System.out.println("8-Sabotage");
        System.out.println("9-Additional Action Point");
        System.out.println("10-Avancar Turno");
        System.out.println("11-Gravar Joguito");
        System.out.println("12-sair");

        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
            
        value=s.nextInt();
        switch(value){
            case 1: j.archersAttack();break;
            case 2:j.boilingWaterAttack();break;
            case 3:j.closeCombatAttack();break;
            case 4:j.coupure();break;
            case 5:j.rallyTroops();break;
            case 6:j.tunnelMovement();break;
            case 7:j.supplyRaid();break;
            case 8:j.sabotage();break;
            case 9:j.additionalActionPoints();break;
            case 10:j.endOfActionSelection();break;
            case 11:gravar();break;
            case 12:sair=true;gravar();break;
        }
    } 
    private void getUserInputAwaitTrackArchers(){
        int value;
        System.out.println("Choose the track that you want to attack: ");
        System.out.println("1-Ladder");
        System.out.println("2-Battering ram");
        System.out.println("3-Siege Tower");
        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
            
        value=s.nextInt();
        j.applyArchersAttack(value);
    }
    private void getUserInputAwaitGiveExtraSupplies(){
        int value;
        boolean choice=false;
        System.out.println("Give extra supplies to obtain +1 DRM");
        System.out.println("1-Yes");
        System.out.println("2-No");
        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();     
        value=s.nextInt();
        if(value==1)
            choice=true;
        j.makeSpeech(choice);
        
    }
    private void getUserInputAwaitSpendSuppliesOrMorale(){
        int value;
        System.out.println("1-Spend Morale ");
        System.out.println("2-Spend Supplies");
         while(!s.hasNextInt()) s.next();     
        value=s.nextInt();
        j.spendSuppliesOrMorale(value);
        
    }
    private void getUserInputAwaitTrackBoiling(){
        int value;
        System.out.println("Choose the track that you want to attack: ");
        System.out.println("1-Ladder");
        System.out.println("2-Battering ram");
        System.out.println("3-Siege Tower");
        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
            
        value=s.nextInt();
        j.applyBoilingWaterAttack(value);
    }
    private void getUserInputAwaitTunnelMovement(){
        int value,move;
        System.out.println("Choose direction: ");
        System.out.println("1-Enemy Line");
        System.out.println("2-Castle");
        System.out.print("> ");
        while(!s.hasNextInt())s.next();
        move=s.nextInt();
        System.out.println("Choose the type of movement: ");
        System.out.println("1-Free Movement");
        System.out.println("2-Fast Movement");
        System.out.print("> ");
        
        while(!s.hasNextInt()) s.next();
        value=s.nextInt();
        j.applyTunnelMovement(value, move);
    }
    
    void gravar() {
        try {
            FicheirosDeObjectos.guardaEmFicheiroDeObjectos(j, "jogo.bin");
        } catch (IOException e) {
            System.out.println(" Erro ao gravar em ficheiro de objectos " + e);
            return;
        }
        System.out.println(" Gravou com sucesso em ficheiro de objectos ");
    }
    void ler() {
        System.out.println("Ler jogo de ficheiro de objectos: ");
        try {
            Jogo outro = FicheirosDeObjectos.carregaDeFicheiroDeObjectos("jogo.bin");
            System.out.println("sockets2");
            j = outro;
        } catch (IOException | ClassNotFoundException e) {

            System.out.println("Erro ao ler o ficheiro ... ");
            System.out.println("Ficheiro jogo.bin: " + e);
            System.out.println();
        }
    }   
    public void run(){

        while(! ( j.getEstado() instanceof GameOver) && !(j.getEstado() instanceof GameWin)&& !sair){
            
                 
            for(int clear = 0; clear < 100; clear++) {
                System.out.println();
            }
            
               if(j.getMsgLog().size()>0){
                
                System.out.println();
                   
                for(String msg:j.getMsgLog()){
                    System.out.println("---> " + msg);
                }     
                
                j.clearMsgLog();
                
            }
       
            if(j.getEstado()instanceof AwaitActionSelection){
                getUserInputAwaitActionSelection();
            }else if(j.getEstado()instanceof AwaitBeginning){
                getUserInputWhileAwaitingBeginning();
            }else if(j.getEstado()instanceof AwaitGiveExtraSupplies){
                getUserInputAwaitGiveExtraSupplies();
            }else if(j.getEstado() instanceof AwaitTopCard){
                getUserAwaitTopCard();
            }else if(j.getEstado() instanceof AwaitTrackArchers){
                getUserInputAwaitTrackArchers();
            }else if(j.getEstado() instanceof AwaitSpendSuppliesOrMorale){
                getUserInputAwaitSpendSuppliesOrMorale();
            }else if(j.getEstado() instanceof AwaitTrackBoiling){
                getUserInputAwaitTrackBoiling();
            }else if(j.getEstado() instanceof AwaitMovementTunnel){
                getUserInputAwaitTunnelMovement();
            }

        }
        
        System.out.println();
        if(j.getEstado() instanceof GameOver)
            System.out.println("************** Game over *****************");
        else
            if(sair)
                System.out.println("Não se va Embora ! Este Jogo deu trabalhito a fazer!!! Tente de Novo");
        else
            System.out.println("PARABENS GANHOU O JOGO! :) :)");
       

    }

  

}
