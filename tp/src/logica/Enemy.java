/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;

/**
 *
 * @author joao
 */
public class Enemy implements Serializable{
    private int trackLeft;
    private int trackCenter;
    private int trackRight;
    private int trebuchets;
    private int strengthLeft=2;
    private int strengthCenter=3;
    private int strengthRight=4;

    public Enemy() {
        this.trackLeft = 4;
        this.trackCenter = 4;
        this.trackRight = 4;
        this.trebuchets = 3;
    }

    
    public int getTrebuchets() {
        return trebuchets;
    }

    public void setTrebuchets(int trebuchets) {
        this.trebuchets = trebuchets;
    }

    public int getTrackLeft() {
        return trackLeft;
    }

    public void setTrackLeft(int trackLeft) {
        if(trackLeft<5 && trackLeft>=0)
        this.trackLeft = trackLeft;
        
    }

    public int getTrackCenter() {
        return trackCenter;
    }

    public void setTrackCenter(int trackCenter) {
        if(trackCenter<5 && trackCenter>=0)
        this.trackCenter = trackCenter;
    }

    public int getTrackRight() {
        return trackRight;
    }

    public void setTrackRight(int trackRight) {
        if(trackRight<5 && trackRight>=0)
        this.trackRight = trackRight;
    }
    
    public int[] getSlowest(){
       int[] array=new int[3];
       int[] novo=new int[3];
       
       int maior,pos=0;
       array[0]=getTrackLeft();
       array[1]=getTrackCenter();
       array[2]=getTrackRight();
       maior=array[0];
       
       for(int i=0;i<array.length;i++)
           novo[i]=0;
       for(int i=0;i<array.length;i++){
           
           if(array[i]>maior)
           {
               maior=array[i];
               pos=i;
           }
       }
       novo[pos]=1;

        for(int j=pos+1;j<array.length;j++)
        {
            if(array[pos]==array[j])
            {
                novo[j]=1;
            }
        }
       
       return novo;
  
    }

    public int getStrengthLeft() {
        return strengthLeft;
    }

    public int getStrengthCenter() {
        return strengthCenter;
    }

    public int getStrengthRight() {
        return strengthRight;
    }

    @Override
    public String toString() {
        return "Enemy{" + "Ladder=" + trackLeft + ", BatteringRam=" + trackCenter + ", Siege Tower=" + trackRight + ", trebuchets=" + trebuchets + '}';
    }
    
}
