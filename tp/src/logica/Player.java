/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;

/**
 *
 * @author joao
 */
public class Player implements Serializable{
    private int morale;
    private int wallStrength;
    private int supplies;
    private int tunel;
    private int carriedSupplies;

    public Player() {
        this.morale = 4;
        this.wallStrength = 4;
        this.supplies = 4;
        this.tunel = 1;
        this.carriedSupplies = 0;
    }

    public int getTunel() {
        return tunel;
    }    

    public void setTunel(int tunel) {
        if(tunel >0 && tunel<5)
            this.tunel = tunel;
    }

    public int getMorale() {
        return morale;
    }

    public void setMorale(int morale) {
        if(morale>=0 && morale<5)
        this.morale = morale;
    }

    public int getWallStrength() {
        return wallStrength;
    }

    public void setWallStrength(int wallStrength) {
        if(wallStrength>=0 && wallStrength<5)
        this.wallStrength = wallStrength;
    }

    public int getSupplies() {
        return supplies;
    }

    public void setSupplies(int supplies) {
        if(supplies>=0 && supplies<5)
        this.supplies = supplies;
        if(supplies>4)
            this.supplies=4;
    }

    public int getCarriedSupplies() {
        return carriedSupplies;
    }

    public void setCarriedSupplies(int carriedSupplies) {
        if(carriedSupplies>=0 && carriedSupplies<3)
        this.carriedSupplies = carriedSupplies;
    }

    @Override
    public String toString() {
        return "Player{" + "morale=" + morale + ", wallStrength=" + wallStrength + ", supplies=" + supplies + ", tunel=" + tunel + ", carriedSupplies=" + carriedSupplies + '}';
    }
    
}
