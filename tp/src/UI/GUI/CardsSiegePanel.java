/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import logica.ObservableGame;

/**
 *
 * @author luis_
 */
public class CardsSiegePanel extends JPanel implements Constants{
    private ObservableGame observableGame;
    

    private MLeftPanel leftPanel;
    private MRightPanel rightPanel;
    private MCenterPanel centerPanel;
    private LogPanel lp;
   
    public CardsSiegePanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
       
        
        setOpaque(false);
        setupComponents();
        setupLayout();
        
    }
    
    private void setupComponents(){

        leftPanel=new MLeftPanel(observableGame);
        rightPanel=new MRightPanel(observableGame);
        centerPanel=new MCenterPanel(observableGame);
        lp=new LogPanel(observableGame);
      
    }
    
    private void setupLayout(){
        setLayout(new BorderLayout());
        
       
        add(leftPanel, BorderLayout.WEST);
        add(rightPanel, BorderLayout.EAST);
        add(centerPanel,BorderLayout.CENTER);
        add(lp,BorderLayout.SOUTH);
        validate();
    }
    

    
    
    
}
