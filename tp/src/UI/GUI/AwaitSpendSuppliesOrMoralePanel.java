/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitSpendSuppliesOrMorale;

/**
 *
 * @author luis_
 */
public class AwaitSpendSuppliesOrMoralePanel extends JPanel implements Observer{
    private ObservableGame observableGame;
     private JButton spendMoraleBtn,spendSuppliesBtn;

    public AwaitSpendSuppliesOrMoralePanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        spendMoraleBtn=new JButton("Spend Morale");
        spendSuppliesBtn=new JButton("Spend Supplies");
        setupComponents();
        add(spendMoraleBtn);
        add(spendSuppliesBtn);
        setupLayout();
        
        spendMoraleBtn.addActionListener(new applySpendMoraleListener());
        spendSuppliesBtn.addActionListener(new applySpendSuppliesListener());
        update(observableGame,null);
    }
    private void setupComponents(){
        spendMoraleBtn.setPreferredSize(new Dimension(200,30));
        spendMoraleBtn.setMinimumSize(new Dimension(200, 30));
        spendMoraleBtn.setMaximumSize(new Dimension(200, 30));
        spendMoraleBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        spendSuppliesBtn.setPreferredSize(new Dimension(200,30));
        spendSuppliesBtn.setMinimumSize(new Dimension(200, 30));
        spendSuppliesBtn.setMaximumSize(new Dimension(200, 30));
        spendSuppliesBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    private void setupLayout(){
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainer = Box.createHorizontalBox();

        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        JLabel question =new JLabel("You want to spend morale or supplies: ");   
        question.setAlignmentX(Component.CENTER_ALIGNMENT);
        question.setFont(new Font("Arial", Font.BOLD, 18));
        buttonsContainerCenter.add(question);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(spendMoraleBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(spendSuppliesBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));

        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
        
        add(buttonsContainer, BorderLayout.CENTER);
    } 

    @Override
    public void update(Observable o, Object arg) {
        setVisible(observableGame.getEstado() instanceof AwaitSpendSuppliesOrMorale);
    }
    
    private class applySpendMoraleListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.spendSuppliesOrMorale(1);
        }        
    }
    private class applySpendSuppliesListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.spendSuppliesOrMorale(2);
        }        
    }
}
