/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import static UI.GUI.Constants.IMG_CARD2;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;

/**
 *
 * @author joao
 */
public class PlayerCardPanel extends JPanel implements Constants,Observer{
    private ObservableGame observableGame;
    private JLabel image;
    private List<Rectangle> squares = new ArrayList<Rectangle>();
    private Rectangle tunel,cSupplies;
    private int tokenSizeX = 56, tokenSizeY = 58;
    private int subir=76;


    
    public PlayerCardPanel(ObservableGame observableGame){
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        tunel=new Rectangle(131,14+(5*subir)-1,tokenSizeX-9,tokenSizeY-5);
        cSupplies=new Rectangle(340,14+(4*subir)+19,tokenSizeX-5,tokenSizeY-5);
        setupComponents();
        add(image);
    }
    
   
    
    private void setupComponents()
    { 
        image = new JLabel();
        image.setIcon(new ImageIcon(Common.getScaledImg(IMG_PLAYER, 300, 450)));
        squares.add(new Rectangle(137, 14, tokenSizeX, tokenSizeY)); // leftTrack
        squares.add(new Rectangle(232, 14, tokenSizeX, tokenSizeY)); // CenterTrack
        squares.add(new Rectangle(329, 14, tokenSizeX, tokenSizeY)); // RightTrack REAL   
        //tunel.add(new Rectangle(137,14+(4*subir),tokenSizeX,tokenSizeY));
    }
    
    private void atualizaTunnel()
    {
        int x=131,y=14+(5*subir)-1;
        int space=tokenSizeX-9;
        
        if(observableGame.getDataGame().getJogador().getTunel()==2)
            x=x+space;
        if(observableGame.getDataGame().getJogador().getTunel()==3)
            x=x+(2*space);
        if(observableGame.getDataGame().getJogador().getTunel()==4)
            x=x+(3*space)+4;
        tunel.setLocation(x, y);
    }
    
    private void atualizaLeft()
    {
        int x=137,y=14;
        if(observableGame.getDataGame().getJogador().getWallStrength()==3)
            y+=subir;
        if(observableGame.getDataGame().getJogador().getWallStrength()==2)
            y=y+subir+subir-2;
        if(observableGame.getDataGame().getJogador().getWallStrength()==1)
            y=y+(3*subir)-4;
        squares.get(0).setLocation(x, y);
    }
    private void atualizaCenter()
    {
        int x=232,y=14;
      
        if(observableGame.getDataGame().getJogador().getMorale()==3)
            y+=subir;
        if(observableGame.getDataGame().getJogador().getMorale()==2)
            y=y+subir+subir-2;
        if(observableGame.getDataGame().getJogador().getMorale()==1)
            y=y+(3*subir)-4;
        squares.get(1).setLocation(x, y);
    }
    private void atualizaRight()
    {
        int x=329,y=14;
        if(observableGame.getDataGame().getJogador().getSupplies()==3)
            y+=subir;
        if(observableGame.getDataGame().getJogador().getSupplies()==2)
            y=y+(2*subir)-2;
        if(observableGame.getDataGame().getJogador().getSupplies()==1)
            y=y+(3*subir)-4;
        squares.get(2).setLocation(x, y);
    }
    
    private void atualizaC(){
        int x=340,y=33+(4*subir);
        if(observableGame.getDataGame().getJogador().getCarriedSupplies()==2)
            cSupplies.setLocation(x, y);
        if(observableGame.getDataGame().getJogador().getCarriedSupplies()==1)
            cSupplies.setLocation(x, y+tokenSizeY-2);
            
    }
    
    protected void paintChildren(Graphics g) 
    {
        int i = 0;
        
        super.paintChildren(g);
        
        Graphics2D g2 = (Graphics2D) g;
        
        final float dash1[] = {0.1f};
        final BasicStroke dashed =  new BasicStroke(4f,
                                                    BasicStroke.CAP_SQUARE,
                                                    BasicStroke.JOIN_ROUND,
                                                    0.2f, dash1, 0.0f);
        
        
        for (Rectangle rect : squares) {
            
            g2.setColor(new Color(1, 1, 1, 0.15f));
            g2.fill(rect);
            g2.setStroke(dashed);
            g2.setColor(Color.GREEN);
            if(observableGame.getDataGame().getJogador().getWallStrength()>0 && i==0)  
                g2.draw(rect);
            if(observableGame.getDataGame().getJogador().getMorale()>0 && i==1)  
                g2.draw(rect);
            if(observableGame.getDataGame().getJogador().getSupplies()>0 && i==2)  
                g2.draw(rect);
            i++;
        }
        if(observableGame.getDataGame().getJogador().getMorale()==0 ||observableGame.getDataGame().getJogador().getSupplies()==0 || observableGame.getDataGame().getJogador().getWallStrength()==0)
            g2.drawRect(232,14+(4*subir)-2,tokenSizeX,tokenSizeY);
        g2.setColor(Color.CYAN);
        g2.setStroke(dashed);
        g2.draw(tunel);
        
        g2.setColor(Color.ORANGE);
        if(observableGame.getDataGame().getJogador().getCarriedSupplies()>0)
            g2.draw(cSupplies);
        
    }
    
    @Override
    public void update(Observable o, Object arg) {
        atualizaLeft();
        atualizaCenter();
        atualizaRight();
        atualizaTunnel();
        atualizaC();

        revalidate();
    }
}
