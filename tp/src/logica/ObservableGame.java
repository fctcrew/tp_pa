/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import logica.estados.IEstado;

/**
 *
 * @author luis_
 */
public class ObservableGame extends Observable{
    
    private Jogo jogo ;

    public ObservableGame() {
        this.jogo = new Jogo();
    }

    public Jogo getJogo() {
        return jogo;
    }

    public void setJogo(Jogo jogo) {
        this.jogo = jogo;
        
        setChanged();
        notifyObservers();
    }
    public Datagame getDataGame()
    {
        return jogo.getDatagame();
    }
    
    public IEstado getEstado(){
        return jogo.getEstado();
    }
    
    
    //duplicar diz o navais

    @Override
    public String toString() {
        return jogo.toString(); 
    }
    
    //cuidado
    
    public List<String> getLogs(){
        return jogo.getMsgLog();
    }
     public void comecarJogo(){
        jogo.comecarJogo();
        setChanged();
        notifyObservers();
    }
    public void rallyTroops(){
        jogo.rallyTroops();
        setChanged();
        notifyObservers();
    }
    public void drawTopCard(){
        jogo.drawTopCard();
        setChanged();
        notifyObservers();
    }
    
    public void coupure(){
        jogo.coupure();
        setChanged();
        notifyObservers();
    }
    public void supplyRaid(){
        jogo.supplyRaid();
        setChanged();
        notifyObservers();
    }
    public void sabotage(){
        jogo.sabotage();
        setChanged();
        notifyObservers();
    }
    public void makeSpeech(boolean choice){
        jogo.makeSpeech(choice);
        setChanged();
        notifyObservers();
    }
    public void closeCombatAttack(){
        jogo.closeCombatAttack();
        setChanged();
        notifyObservers();
    }
    public void additionalActionPoints(){
        jogo.additionalActionPoints();
        setChanged();
        notifyObservers();
    }
    public void spendSuppliesOrMorale(int choice){
        jogo.spendSuppliesOrMorale(choice);
        setChanged();
        notifyObservers();
    }
    public void boilingWaterAttack(){
        jogo.boilingWaterAttack();
        setChanged();
        notifyObservers();
    }
    public void applyBoilingWaterAttack(int choice){
        jogo.applyBoilingWaterAttack(choice);
        setChanged();
        notifyObservers();
    }
    
    public void archersAttack(){
        jogo.archersAttack();
        setChanged();
        notifyObservers();
    }
    public void applyArchersAttack(int choice){
        jogo.applyArchersAttack(choice);
        setChanged();
        notifyObservers();
    }
    public void tunnelMovement(){
        jogo.tunnelMovement();
        setChanged();
        notifyObservers();
    }
    public void applyTunnelMovement(int choice,int move){
        jogo.applyTunnelMovement(choice, move);
        setChanged();
        notifyObservers();
    }
    public void endOfActionSelection(){
        jogo.endOfActionSelection();
        setChanged();
        notifyObservers();
    }
    
    public void resetJogo(){
        jogo.resetJogo();
        setChanged();
        notifyObservers();
    }
    
    
    
    
    
}
