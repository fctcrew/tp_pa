/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitBeginning;

/**
 *
 * @author luis_
 */
public class AwaitBeginningPanel extends JPanel implements Observer{
     private ObservableGame observableGame;
     
    private JButton startButton;
    

    public AwaitBeginningPanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        
        startButton = new JButton("Start");
        
        startButton.setPreferredSize(new Dimension(300, 100));
        startButton.setMinimumSize(new Dimension(300, 100));
        startButton.setMaximumSize(new Dimension(300, 100));
    
        add(startButton);
   
        setupLayout();
  
        startButton.addActionListener(new ComecarJogoListener());
        
        update(observableGame, null);
    }
    
    private void setupLayout()
    {
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainer = Box.createHorizontalBox();

        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        buttonsContainerCenter.add(startButton);
        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
     
        add(buttonsContainer, BorderLayout.CENTER);
    }
     
    @Override
    public void update(Observable o, Object arg) {
        setVisible(observableGame.getEstado() instanceof AwaitBeginning );  
    }
    
     class ComecarJogoListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.comecarJogo();
        }        
    }
    
}
