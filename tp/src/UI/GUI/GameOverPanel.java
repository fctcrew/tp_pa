/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitTrackArchers;
import logica.estados.GameOver;

/**
 *
 * @author luis_
 */
public class GameOverPanel extends JPanel implements Observer {
   
    private ObservableGame observableGame;
    private JButton VoltarBtn;

    public GameOverPanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        setVisible(observableGame.getEstado() instanceof GameOver);

        VoltarBtn=new JButton("Voltar a Jogar!!");
      
        setupComponents();
        add(VoltarBtn);
        setupLayout();
        VoltarBtn.addActionListener(new ApplyVoltarListener());
       
        update(observableGame,null);
    }
    
    private void setupComponents(){ 
        VoltarBtn.setPreferredSize(new Dimension(200,30));
        VoltarBtn.setMinimumSize(new Dimension(200, 30));
        VoltarBtn.setMaximumSize(new Dimension(200, 30));
        VoltarBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    
    private void setupLayout(){
       Box buttonsContainerCenter = Box.createVerticalBox();
       Box buttonsContainer = Box.createHorizontalBox();
  
        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        JLabel question =new JLabel("Perdeu Drasticamente este Jogo!!!\n Seja Mais Aplicado!!\n Jogar mais um joguito???");   
        question.setAlignmentX(Component.CENTER_ALIGNMENT);
        question.setFont(new Font("Arial", Font.BOLD, 18));
        buttonsContainerCenter.add(question);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(VoltarBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
      

        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
           
        add(buttonsContainer, BorderLayout.CENTER);
   }

    @Override
    public void update(Observable o, Object arg) {
        setVisible(observableGame.getEstado() instanceof GameOver);
    }
     class ApplyVoltarListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.resetJogo();
        }        
    }
    
}
