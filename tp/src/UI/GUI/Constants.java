/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.Color;
import java.awt.Image;

/**
 *
 * @author luis_
 */
public interface Constants {
    public static final Image IMG_CARD1= Common.getImage("IMAGENS/Carta1.png");
    public static final Image IMG_CARD2= Common.getImage("IMAGENS/Carta2.png");
    public static final Image IMG_CARD3= Common.getImage("IMAGENS/Carta3.png");
    public static final Image IMG_CARD4= Common.getImage("IMAGENS/Carta4.png");
    public static final Image IMG_CARD5= Common.getImage("IMAGENS/Carta5.png");
    public static final Image IMG_CARD6= Common.getImage("IMAGENS/Carta6.png");
    public static final Image IMG_CARD7= Common.getImage("IMAGENS/Carta7.png");
    

    public static final Image IMG_PLAYER= Common.getImage("IMAGENS/CartaPlayer.png");
    public static final Image IMG_ENEMY= Common.getImage("IMAGENS/CartaEnemy.png");
    public static final Image IMG_CAPA= Common.getImage("IMAGENS/Capa.png");
    public static final Image IMG_FUNDO= Common.getImage("IMAGENS/bck.png");

    public static final int BIG_SIZE = 50;
    public static final int MEDIUM_SIZE = 30;
    public static final int LOW_SIZE = 22;
    
    public static final String TITLE_FONT = "Sketch Gothic School";
    public static final String FONT = "Maiandra GD";
    public static final Color FONT_COLOR = Color.BLACK;
}
