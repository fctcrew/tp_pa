/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitActionSelection;


/**
 *
 * @author luis_
 */
 public class AwaitActionSelectionPanel extends JPanel implements Observer{

   private ObservableGame observableGame;
   private JButton archersBtn,boilingBtn,closeCombatBtn;
   private JButton coupureBtn,rallyBtn,tunnelBtn,supplyRaidBtn;
   private JButton sabotageBtn,additionalBtn,avancaTurnoBtn;

    public AwaitActionSelectionPanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        setVisible(observableGame.getEstado() instanceof AwaitActionSelection);
        archersBtn = new JButton("Archers Attack");
        boilingBtn=new JButton("Boiling Water ");
        closeCombatBtn=new JButton("Close Combat");
        coupureBtn=new JButton("Coupure");
        rallyBtn=new JButton("Rally Troops");
        tunnelBtn=new JButton("Tunnel Movement");
        supplyRaidBtn=new JButton("Supply Raid");
        sabotageBtn=new JButton("Sabotage");
        additionalBtn=new JButton("Additional Action Point");
        avancaTurnoBtn=new JButton("Avancar turno");
        setupComponents();
        
        add(archersBtn);
        add(boilingBtn);
        add(closeCombatBtn);
        add(coupureBtn);
        add(rallyBtn);
        add(tunnelBtn);
        add(supplyRaidBtn);
        add(sabotageBtn);
        add(additionalBtn);
        add(avancaTurnoBtn);
        setupLayout();
        archersBtn.addActionListener(new ArchersAttackListener());
        boilingBtn.addActionListener(new BoilingAttackListener());
        closeCombatBtn.addActionListener(new CloseCombatAttackListener());
        coupureBtn.addActionListener(new CoupureListener());
        rallyBtn.addActionListener(new RallyTroopsListener());
        tunnelBtn.addActionListener(new TunnelMovementListener());
        supplyRaidBtn.addActionListener(new SupplyRaidListener());
        sabotageBtn.addActionListener(new SabotageListener());
        additionalBtn.addActionListener(new AdditionalActionPointListener());
        avancaTurnoBtn.addActionListener(new AvancaTurnoListener());
        update(observableGame,null);
        
        
        
    }
    
        private void setupComponents()
    { 
        archersBtn.setPreferredSize(new Dimension(200,30));
        archersBtn.setMinimumSize(new Dimension(200, 30));
        archersBtn.setMaximumSize(new Dimension(200, 30));
        
        boilingBtn.setPreferredSize(new Dimension(200,30));
        boilingBtn.setMinimumSize(new Dimension(200, 30));
        boilingBtn.setMaximumSize(new Dimension(200, 30));
        
        closeCombatBtn.setPreferredSize(new Dimension(200,30));
        closeCombatBtn.setMinimumSize(new Dimension(200, 30));
        closeCombatBtn.setMaximumSize(new Dimension(200,30));
                
        coupureBtn.setPreferredSize(new Dimension(200,30));
        coupureBtn.setMinimumSize(new Dimension(200, 30));
        coupureBtn.setMaximumSize(new Dimension(200, 30));
                
        rallyBtn.setPreferredSize(new Dimension(200,30));
        rallyBtn.setMinimumSize(new Dimension(200, 30));
        rallyBtn.setMaximumSize(new Dimension(200, 30));
                
        tunnelBtn.setPreferredSize(new Dimension(200,30));
        tunnelBtn.setMinimumSize(new Dimension(200, 30));
        tunnelBtn.setMaximumSize(new Dimension(200, 30));
                
        supplyRaidBtn.setPreferredSize(new Dimension(200,30));
        supplyRaidBtn.setMinimumSize(new Dimension(200, 30));
        supplyRaidBtn.setMaximumSize(new Dimension(200, 30));
                
        sabotageBtn.setPreferredSize(new Dimension(200,30));
        sabotageBtn.setMinimumSize(new Dimension(200, 30));
        sabotageBtn.setMaximumSize(new Dimension(200, 30));
                
        additionalBtn.setPreferredSize(new Dimension(200,30));
        additionalBtn.setMinimumSize(new Dimension(200, 30));
        additionalBtn.setMaximumSize(new Dimension(200, 30));
        
        avancaTurnoBtn.setPreferredSize(new Dimension(200,30));
        avancaTurnoBtn.setMinimumSize(new Dimension(200, 30));
        avancaTurnoBtn.setMaximumSize(new Dimension(200, 30));
    }

   private void setupLayout(){
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainerCenter2 = Box.createVerticalBox();

        Box buttonsContainer = Box.createHorizontalBox();
        
        
        
        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        buttonsContainerCenter.add(archersBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(boilingBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(closeCombatBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(coupureBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(rallyBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter2.add(tunnelBtn);
        buttonsContainerCenter2.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter2.add(supplyRaidBtn);
        buttonsContainerCenter2.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter2.add(sabotageBtn);
        buttonsContainerCenter2.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter2.add(additionalBtn);
        buttonsContainerCenter2.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter2.add(avancaTurnoBtn);
        buttonsContainerCenter2.add(Box.createRigidArea(new Dimension(10, 20)));

        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(buttonsContainerCenter2);

        buttonsContainer.add(Box.createHorizontalGlue());
                
        
        //add(lbTitle, BorderLayout.NORTH);        
        add(buttonsContainer, BorderLayout.CENTER);
   }

    @Override
    public void update(Observable o, Object arg) {
      setVisible(observableGame.getEstado() instanceof AwaitActionSelection );  
    }
    
    class ArchersAttackListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.archersAttack();
        }        
    }
    
    private class BoilingAttackListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.boilingWaterAttack();
        }        
    }
    
    class CloseCombatAttackListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.closeCombatAttack();
        }        
    }
    class CoupureListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.coupure();
        }        
    }
    class SabotageListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.sabotage();
        }        
    }
    class RallyTroopsListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.rallyTroops();
        }        
    }
    class TunnelMovementListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.tunnelMovement();
        }        
    }
    class SupplyRaidListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.supplyRaid();
        }        
    }
    class AdditionalActionPointListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.additionalActionPoints();
        }  
    }
    class AvancaTurnoListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.endOfActionSelection();
        }  
    }
}
