/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author joao
 */
public class FicheirosDeObjectos {
        public static Jogo carregaDeFicheiroDeObjectos(String nomeFicheiro) throws IOException, ClassNotFoundException
    {
        Jogo jogo;
        ObjectInputStream in = null;
        
        try{
            in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            jogo = (Jogo)in.readObject();
        }finally{
            if(in!=null){
                in.close();
            }
        }
        
        
        return jogo;
    }

    public static void guardaEmFicheiroDeObjectos(Jogo jogo, String nomeFicheiro)throws IOException
    {   
        ObjectOutputStream out = null;
                
        try{
            out = new ObjectOutputStream(new FileOutputStream(nomeFicheiro));
            out.writeObject(jogo);
        }finally{
            if(out != null){
                try {
                    out.close();
                } catch (IOException e) {}
            }
        }

    }
}
