/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitTrackArchers extends EstadoAdapter{

    public AwaitTrackArchers(Datagame data) {
        super(data);
    }
    
    @Override
     public IEstado applyArchersAttack(int value)
     {
        if(value==1 && (getDatagame().getEnemy().getTrackLeft()==0 ||  getDatagame().getEnemy().getTrackLeft()==4))
        {getDatagame().erro();return new AwaitActionSelection(getDatagame());}
        
        if(value==2 && (getDatagame().getEnemy().getTrackCenter()==0 || getDatagame().getEnemy().getTrackCenter()==4))
        { getDatagame().erro();return new AwaitActionSelection(getDatagame());}
        
        if(value==3 && (getDatagame().getEnemy().getTrackRight()==0 || getDatagame().getEnemy().getTrackRight()==4))
        {getDatagame().erro();return new AwaitActionSelection(getDatagame());}
        
         
         getDatagame().getDado().roll();
         int dice=getDatagame().getDado().getNumero();
         
         getDatagame().addMsgLog("Dado:"+getDatagame().getDado().getNumero());
         if(value==1){
            getDatagame().addMsgLog("DRM Ladder:"+getDatagame().getDrmLadder());
            if(dice+getDatagame().getDrmLadder()>getDatagame().getEnemy().getStrengthLeft()){
                getDatagame().getEnemy().setTrackLeft(getDatagame().getEnemy().getTrackLeft()+1);
            }
         }
         if(value==2){
            getDatagame().addMsgLog("DRM Battering ram:"+getDatagame().getDrmBatteringRam());
            if(dice+getDatagame().getDrmBatteringRam()>getDatagame().getEnemy().getStrengthCenter() &&  getDatagame().getEnemy().getTrackCenter()<4){
                getDatagame().getEnemy().setTrackCenter(getDatagame().getEnemy().getTrackCenter()+1);
            }
         }
         if(value==3){
            getDatagame().addMsgLog("DRM Siege Tower:"+getDatagame().getDrmSiegeTower());
            if(dice+getDatagame().getDrmSiegeTower()>getDatagame().getEnemy().getStrengthRight() &&  getDatagame().getEnemy().getTrackRight()<4) {
                getDatagame().getEnemy().setTrackRight(getDatagame().getEnemy().getTrackRight()+1);
            }   
         }
         getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
         return new AwaitActionSelection(getDatagame()); 
     }
    
}
