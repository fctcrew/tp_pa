/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

/**
 *
 * @author joao e luis
 */
public interface IEstado {
    IEstado comecarJogo();
    IEstado rallyTrops();
    IEstado drawTopCard();
    IEstado coupure();
    IEstado supplyRaid();
    IEstado sabotage();
    IEstado closeCombatAttack();
    IEstado makeSpeech(boolean choice);
    IEstado additionalActionPoints();
    IEstado spendSuppliesOrMorale(int choice);
    IEstado applyArchersAttack(int value);
    IEstado archersAttack();
    IEstado boilingWaterAttack();
    IEstado applyBoilingWaterAttack(int choice);
    IEstado tunnelMovement();
    IEstado applyTunnelMovement(int choice,int move);  
    IEstado endOfActionSelection();
    IEstado resetJogo();
}
