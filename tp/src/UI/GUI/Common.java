/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import static UI.GUI.Constants.FONT;
import static UI.GUI.Constants.FONT_COLOR;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;

/**
 *
 * @author luis_
 */
public class Common {
    public static final BufferedImage getImage(String image)  
    {  
        BufferedImage img = null;

        try{
            img = ImageIO.read(Resources.getResourceFile(image));
        }catch(IOException e){
            System.err.println(e.getMessage());
        } 

        return img;  
    }

    public static final  Image getScaledImg(Image img, int width, int height)
    {
        return img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
    }
    public static final void setFont(JLabel component, int fontSize)
    {
        component.setFont (new Font(FONT, Font.BOLD, fontSize));
        component.setForeground(FONT_COLOR);
    }
    
    public static final void setFont(JButton component, int fontSize)
    {
        component.setFont (new Font(FONT, Font.BOLD, fontSize));
        component.setForeground(FONT_COLOR);
    }
    
    public static final void setFont(JSlider component, int fontSize)
    {
        component.setFont (new Font(FONT, Font.BOLD, fontSize));
        component.setForeground(FONT_COLOR);
    }
}
