
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;


import java.awt.BorderLayout;
import javax.swing.Box;
import javax.swing.JPanel;
import logica.ObservableGame;

/**
 *
 * @author joao
 */
public class MCenterPanel extends JPanel implements Constants{
    private  ObservableGame observableGame;
        
        
    private AwaitBeginningPanel awaitBeginningPanel;
    private AwaitTopCardPanel awaitTopCardPanel;
    private GameOverPanel gameOverPanel;
    private AwaitActionSelectionPanel awaitActionSelectionPanel;
    private AwaitGiveExtraSuppliesPanel awaitGiveExtraSuppliesPanel;
    private AwaitSpendSuppliesOrMoralePanel awaitSpendSuppliesOrMoralePanel;
    private AwaitTrackArchersPanel awaitTrackArchersPanel;
    private AwaitTrackBoilingPanel awaitTrackBoilingPanel;
    private AwaitMovementTunnelPanel awaitMovementTunnelPanel;
    private EventCardPanel eventCardPanel;
    private GameWinPanel gamewinPanel;
    
    public MCenterPanel(ObservableGame g){
        observableGame = g;
        
        setOpaque(false);
        
        setupComponents();
        setupLayout();
    }
    public void setupComponents(){
        awaitBeginningPanel=new AwaitBeginningPanel(observableGame);
        awaitTopCardPanel=new AwaitTopCardPanel(observableGame);
        awaitActionSelectionPanel=new AwaitActionSelectionPanel(observableGame);
        awaitTrackArchersPanel=new AwaitTrackArchersPanel(observableGame);
        awaitTrackBoilingPanel=new AwaitTrackBoilingPanel(observableGame);
        awaitGiveExtraSuppliesPanel=new AwaitGiveExtraSuppliesPanel(observableGame);
        awaitSpendSuppliesOrMoralePanel=new AwaitSpendSuppliesOrMoralePanel(observableGame);
        awaitMovementTunnelPanel=new AwaitMovementTunnelPanel(observableGame);
        eventCardPanel=new EventCardPanel(observableGame);
        gameOverPanel=new GameOverPanel(observableGame);
        gamewinPanel=new GameWinPanel(observableGame);
    }
    public void setupLayout(){
        Box centerContainer = Box.createVerticalBox();
        
        setLayout(new BorderLayout());
        
        centerContainer.add(Box.createVerticalGlue());
        centerContainer.add(awaitBeginningPanel);
        centerContainer.add(awaitTopCardPanel);
        centerContainer.add(awaitActionSelectionPanel);
        centerContainer.add(awaitTrackArchersPanel);
        centerContainer.add(awaitTrackBoilingPanel);
        centerContainer.add(awaitMovementTunnelPanel);
        centerContainer.add(awaitGiveExtraSuppliesPanel);
        centerContainer.add(awaitSpendSuppliesOrMoralePanel);
        centerContainer.add(gameOverPanel);
        centerContainer.add(gamewinPanel);
        centerContainer.add(Box.createVerticalGlue());
        
        add(eventCardPanel, BorderLayout.NORTH);
        add(centerContainer, BorderLayout.CENTER);
    }
}
