/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import java.util.Collections;
import logica.Datagame;

/**
 *
 * @author luis_
 */
public class AwaitActionSelection extends EstadoAdapter{
    
    public AwaitActionSelection(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado rallyTrops() {
        getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0 && !getDatagame().isBlockActions() && getDatagame().getJogador().getMorale()<4)
            return new AwaitGiveExtraSupplies(this.getDatagame());
        else
            getDatagame().erro();
        return this;
        
    }
    
    @Override
    public IEstado closeCombatAttack(){
        getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0){ 
            getDatagame().CloseCombatArea();
            if(getDatagame().getGameover()==1)
                return new GameOver(getDatagame());
        }
        return this;
    }
    
    @Override
    public IEstado additionalActionPoints(){
        getDatagame().clearMsgLog();
        if(!getDatagame().isAddActionPointsUsed())
            return new AwaitSpendSuppliesOrMorale(this.getDatagame());
        else
            getDatagame().erro();
        return this;
    }
  
    @Override
    public IEstado archersAttack(){
        getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0 &&!getDatagame().isBlockActions())
            return new AwaitTrackArchers(getDatagame());
        else
            getDatagame().erro();
        return this;
    }
    
    @Override
    public IEstado boilingWaterAttack(){
        getDatagame().clearMsgLog();
        if(!getDatagame().isBoilingUsed() && getDatagame().getActionPointsAtual()>0 && !getDatagame().NoOneCloseCircleSpaces() && !getDatagame().isBlockActions())
            return new AwaitTrackBoiling(getDatagame());
        else
            getDatagame().erro();
        return this;
    }
    
    @Override
    public IEstado coupure(){
        getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0 && getDatagame().getJogador().getWallStrength()<4 && !getDatagame().isBlockActions())
        {
            getDatagame().getDado().roll();
            getDatagame().addMsgLog("Dado:"+getDatagame().getDado().getNumero()+"\nDrm Coupure: "+getDatagame().getDrmCoupure()+"\n");
            if(getDatagame().getDado().getNumero()+getDatagame().getDrmCoupure()>4)
                getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()+1);
            getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
        }
        else
            getDatagame().erro();
        return this;
    }
    
    @Override
    public IEstado supplyRaid(){
        getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0 && getDatagame().getJogador().getTunel()==4 && getDatagame().getJogador().getCarriedSupplies()<2){
            getDatagame().getDado().roll();
            getDatagame().addMsgLog("Dado:"+getDatagame().getDado().getNumero()+"\nDrm supply Raid:"+getDatagame().getDrmRaid()+"\n");
            if(getDatagame().getDado().getNumero()+getDatagame().getDrmRaid()==1)
            {
                getDatagame().getJogador().setTunel(0);
                getDatagame().getJogador().setCarriedSupplies(0);
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return new GameOver(getDatagame());
            }
            else
              if(getDatagame().getDado().getNumero()+getDatagame().getDrmRaid()<5)
                  getDatagame().getJogador().setCarriedSupplies(getDatagame().getJogador().getCarriedSupplies()+1);
                else
                    getDatagame().getJogador().setCarriedSupplies(2);   
            getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
        }
        else
            getDatagame().erro();
        return this;
    }
    
    @Override
    public IEstado sabotage()
    {   getDatagame().clearMsgLog();
        if(getDatagame().getActionPointsAtual()>0 && getDatagame().getJogador().getTunel()==4 && getDatagame().getEnemy().getTrebuchets()>0){
            getDatagame().getDado().roll();
            getDatagame().addMsgLog("Dado:"+getDatagame().getDado().getNumero()+"\n Drm Sabotage: "+getDatagame().getDrmSabotage()+"\n");
            if(getDatagame().getDado().getNumero()+getDatagame().getDrmSabotage()>4){
                getDatagame().getEnemy().setTrebuchets(getDatagame().getEnemy().getTrebuchets()-1);
            }
            else
                if(getDatagame().getDado().getNumero()+getDatagame().getDrmSabotage()==1){
                    getDatagame().getJogador().setTunel(0);
                    getDatagame().getJogador().setCarriedSupplies(0);
                    getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                    if(getDatagame().isTwoOnZero())
                        return new GameOver(getDatagame());
                }
            getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
        }
        else
            getDatagame().erro();
        return this;   
    }
    
    @Override
    public IEstado tunnelMovement(){
        getDatagame().clearMsgLog();
        if(!getDatagame().isBlockActions())
            return new AwaitMovementTunnel(getDatagame());
        else
            getDatagame().erro();
        return this;
    }
    
    @Override
    public IEstado endOfActionSelection(){
        getDatagame().clearMsgLog();
        if(getDatagame().isMandatory())
            return new GameOver(getDatagame());
        if(getDatagame().getJogador().getWallStrength()==0 || getDatagame().getJogador().getMorale()==0 || getDatagame().getJogador().getSupplies()==0)
            return new GameOver(getDatagame());
        if(getDatagame().getBaralho().isEmpty()){

           getDatagame().getJogador().setSupplies(getDatagame().getJogador().getSupplies()-1);
           if(getDatagame().getJogador().getTunel()==4)
           {
               getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
               if(getDatagame().isTwoOnZero())
                    return new GameOver(getDatagame());
           }
           else
            getDatagame().getJogador().setSupplies(getDatagame().getJogador().getCarriedSupplies()+getDatagame().getJogador().getSupplies());
           getDatagame().getJogador().setCarriedSupplies(0);
           getDatagame().getJogador().setTunel(1);
           getDatagame().setDiaAtual(getDatagame().getDiaAtual()+1);
           getDatagame().getBaralho().addAll(getDatagame().GetRemovidasBaralho());
           Collections.shuffle(getDatagame().getBaralho());
           
          
        }
        if(getDatagame().getDiaAtual()==4){
            return new GameWin(getDatagame());
        }
        
        getDatagame().resetDrm();
        getDatagame().setAddActionPointsUsed(false);
        getDatagame().setFreeMovementUsed(false);
        getDatagame().setBlockActions(false);
        getDatagame().setBoilingUsed(false);
        
        if(getDatagame().getJogador().getTunel()==4){
            getDatagame().getDado().roll();
            getDatagame().addMsgLog("Enemy Line Check :\nDado:"+getDatagame().getDado().getNumero()+"\n");
            if(getDatagame().getDado().getNumero()==1){
                getDatagame().getJogador().setTunel(1);
                getDatagame().getJogador().setCarriedSupplies(0);
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
            }
        }
        
        
        return new AwaitTopCard(getDatagame());
       
       
       
    }
}
