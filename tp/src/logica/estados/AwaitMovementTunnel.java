/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitMovementTunnel extends EstadoAdapter{

    public AwaitMovementTunnel(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado applyTunnelMovement(int choice,int move){
        if(getDatagame().getJogador().getTunel()==1 && move==2)
        {getDatagame().erro();return new AwaitActionSelection(getDatagame());}
        if(getDatagame().getJogador().getTunel()==4 && move==1)
        {getDatagame().erro();return new AwaitActionSelection(getDatagame());}
        
        if(choice==1 && !getDatagame().isFreeMovementUsed()){
            if(move==1)
                getDatagame().getJogador().setTunel(getDatagame().getJogador().getTunel()+1);
            if(move == 2)
                getDatagame().getJogador().setTunel(getDatagame().getJogador().getTunel()-1);            
            getDatagame().setFreeMovementUsed(true);
        }
        if(choice==2){
            if(getDatagame().getActionPointsAtual()==0)
            {getDatagame().erro();return new AwaitActionSelection(getDatagame());}
            if(move==1)
                getDatagame().getJogador().setTunel(4);
            if(move==2)
                getDatagame().getJogador().setTunel(1);
            getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);
        }
        if(move==2 && getDatagame().getJogador().getTunel()==1)
        {  
            getDatagame().getJogador().setSupplies(getDatagame().getJogador().getSupplies()+getDatagame().getJogador().getCarriedSupplies());
            getDatagame().getJogador().setCarriedSupplies(0);
        }
        return new AwaitActionSelection(getDatagame());
    }
}
