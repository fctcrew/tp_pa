/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitMovementTunnel;

/**
 *
 * @author luis_
 */
public class AwaitMovementTunnelPanel extends JPanel implements Observer{
    private ObservableGame observableGame;
    private JButton avancaFastBtn,avancaFreeBtn,recuaFreeBtn,recuaFastBtn;
    
    public AwaitMovementTunnelPanel(ObservableGame observableGame) {
       this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        avancaFastBtn=new JButton("Enemy Lines/Fast Movement");
        avancaFreeBtn=new JButton("Enemy Lines/Free Movement");
        recuaFastBtn=new JButton("Castle/Fast Movement");
        recuaFreeBtn=new JButton("Castle/Free Movement");
        setupComponents();
        add(avancaFastBtn);
        add(avancaFreeBtn);
        add(recuaFastBtn);
        add(recuaFreeBtn);
        setupLayout();
        avancaFastBtn.addActionListener(new applyTunnelMovementAvancaFastListener());
        avancaFreeBtn.addActionListener(new applyTunnelMovementAvancaFreeListener());
        recuaFastBtn.addActionListener(new applyTunnelMovementRecuaFastListener());
        recuaFreeBtn.addActionListener(new applyTunnelMovementRecuaFreeListener());

        update(observableGame,null);

    }
    private void setupComponents(){
        avancaFastBtn.setPreferredSize(new Dimension(200,30));
        avancaFastBtn.setMinimumSize(new Dimension(200, 30));
        avancaFastBtn.setMaximumSize(new Dimension(200, 30));
        avancaFastBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        avancaFreeBtn.setPreferredSize(new Dimension(200,30));
        avancaFreeBtn.setMinimumSize(new Dimension(200, 30));
        avancaFreeBtn.setMaximumSize(new Dimension(200, 30));
        avancaFreeBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        recuaFastBtn.setPreferredSize(new Dimension(200,30));
        recuaFastBtn.setMinimumSize(new Dimension(200, 30));
        recuaFastBtn.setMaximumSize(new Dimension(200,30));
        recuaFastBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
                
        recuaFreeBtn.setPreferredSize(new Dimension(200,30));
        recuaFreeBtn.setMinimumSize(new Dimension(200, 30));
        recuaFreeBtn.setMaximumSize(new Dimension(200, 30));
        recuaFreeBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    private void setupLayout(){
        Box buttonsContainerCenter = Box.createVerticalBox();
        Box buttonsContainer = Box.createHorizontalBox();
        
        
        
        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        JLabel question =new JLabel("Choose direction / Choose the type of movement: ");   
        question.setAlignmentX(Component.CENTER_ALIGNMENT);
        question.setFont(new Font("Arial", Font.BOLD, 18));
        buttonsContainerCenter.add(question);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(avancaFreeBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(avancaFastBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(recuaFreeBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(recuaFastBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));

        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
                
        
        //add(lbTitle, BorderLayout.NORTH);        
        add(buttonsContainer, BorderLayout.CENTER);
    }


    @Override
    public void update(Observable o, Object arg) {
       setVisible(observableGame.getEstado() instanceof AwaitMovementTunnel);
    }
    
    private class applyTunnelMovementAvancaFastListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyTunnelMovement(2,1);
        }        
    }
    
   private class applyTunnelMovementAvancaFreeListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyTunnelMovement(1,1);
        }        
    }
    private class applyTunnelMovementRecuaFastListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyTunnelMovement(2,2);
        }        
    }
    private class applyTunnelMovementRecuaFreeListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyTunnelMovement(1,2);
        }        
    }
    
}
