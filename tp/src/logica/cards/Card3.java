/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;



/**
 *
 * @author joao
 */
public class Card3 extends EventCard{
    
        public Card3(Datagame g) {
        super(g);
    }
    
      @Override
    public void Dia1(){
        this.setEventName("Supplies spoiled");
        this.getDatagame().setActionPointsAtual(2);
        
        this.getDatagame().getJogador().setSupplies(getDatagame().getJogador().getSupplies()-1);
        if(getDatagame().isTwoOnZero())
                    return;
        if(getDatagame().getEnemy().getStrengthLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
          if(getDatagame().getEnemy().getTrackLeft()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
       
        }

    }
    
    @Override
    public void Dia2(){
        this.getDatagame().setActionPointsAtual(2);
        this.getDatagame().setBlockActions(true);
        this.setEventName("Bad Weather");

    }
    
    @Override
    public void Dia3(){
        this.setEventName("Boiling Oil");
        this.getDatagame().setActionPointsAtual(2);
        if(this.getDatagame().getEnemy().getTrackLeft()==1)
            this.getDatagame().setDrmLadder(2);
        if(this.getDatagame().getEnemy().getTrackCenter()==1)
            this.getDatagame().setDrmBatteringRam(2);
        if(this.getDatagame().getEnemy().getTrackRight()==1)
            this.getDatagame().setDrmSiegeTower(2);
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
             if(getDatagame().getEnemy().getTrackLeft()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
             }
        }
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                
        }
        

    }
}
