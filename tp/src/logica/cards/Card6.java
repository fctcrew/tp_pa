/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;



/**
 *
 * @author joao
 */
public class Card6 extends EventCard{
    
        public Card6(Datagame g) {
        super(g);
    }
    
      @Override
    public void Dia1(){
        this.setEventName("Cover of Darkness");
        this.getDatagame().setActionPointsAtual(3);
        this.getDatagame().setDrmRaid(1);
        this.getDatagame().setDrmSabotage(1);
        
        int []array=this.getDatagame().getEnemy().getSlowest();
        
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
            this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-array[2]);
             if(getDatagame().getEnemy().getTrackRight()==0){
                    getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                    if(getDatagame().isTwoOnZero())
                    return;
             }
        }

        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-array[1]);
            if(getDatagame().getEnemy().getTrackCenter()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
            }
        }
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-array[0]);
             if(getDatagame().getEnemy().getTrackLeft()==0)
                    getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
  
        
    }
    
    @Override
    public void Dia2(){
        this.getDatagame().setActionPointsAtual(3);
        this.getDatagame().setDrmRaid(1);
        this.getDatagame().setDrmSabotage(1);
        this.getDatagame().setDrmCoupure(1);
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
            if(getDatagame().getEnemy().getTrackLeft()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }

        this.setEventName("Enemy fatigue");
    }
    
    @Override
    public void Dia3(){
        this.setEventName("Rally");
        this.getDatagame().setActionPointsAtual(3);
        this.getDatagame().setDrmCloseCombat(1);

        
        if(this.getDatagame().getEnemy().getTrackLeft()==1)
            this.getDatagame().setDrmLadder(2);
        if(this.getDatagame().getEnemy().getTrackCenter()==1)
            this.getDatagame().setDrmBatteringRam(2);
        if(this.getDatagame().getEnemy().getTrackRight()==1)
            this.getDatagame().setDrmSiegeTower(2);
        
        if(!this.getDatagame().isSiegeTowerRemoved() && getDatagame().getEnemy().getTrackRight()>0){
            this.getDatagame().getEnemy().setTrackRight(this.getDatagame().getEnemy().getTrackRight()-1);
            if(getDatagame().getEnemy().getTrackRight()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
            }
        }
        
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        
      
        
    }
}
