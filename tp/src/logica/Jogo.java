/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;
import java.util.List;
import logica.estados.AwaitBeginning;
import logica.estados.IEstado;

/**
 *
 * @author luis_
 */
public class Jogo implements Serializable{
    
    private Datagame datagame;
    private IEstado estado;

    public Jogo() {
        this.datagame = new Datagame();
        this.estado = new AwaitBeginning(datagame);
    }
    public List<String> getMsgLog()
    {
        return datagame.getMsgLog();
    }

    public void clearMsgLog()
    {
        datagame.clearMsgLog();
    }
    
    public Datagame getDatagame() {
        return datagame;
    }

    public void setDatagame(Datagame datagame) {
        this.datagame = datagame;
    }

    public IEstado getEstado() {
        return estado;
    }

    public void setEstado(IEstado estado) {
        this.estado = estado;
    }
    
    @Override
    public String toString()
    {   
        return datagame.toString();
    }
    
    
    
    // ESTADOS
    public void comecarJogo(){
        setEstado(getEstado().comecarJogo());
    }
    public void rallyTroops(){
        setEstado(getEstado().rallyTrops());
    }
    public void drawTopCard(){
        setEstado(getEstado().drawTopCard());
    }
    
    public void coupure(){
        setEstado(getEstado().coupure());
    }
    public void supplyRaid(){
        setEstado(getEstado().supplyRaid());
    }
    public void sabotage(){
        setEstado(getEstado().sabotage());
    }
    public void makeSpeech(boolean choice){
        setEstado(getEstado().makeSpeech(choice));
    }
    public void closeCombatAttack(){
        setEstado(getEstado().closeCombatAttack());
    }
    public void additionalActionPoints(){
        setEstado(getEstado().additionalActionPoints());
    }
    public void spendSuppliesOrMorale(int choice){
        setEstado(getEstado().spendSuppliesOrMorale(choice));  
    }
    public void boilingWaterAttack(){
        setEstado(getEstado().boilingWaterAttack());
    }
    public void applyBoilingWaterAttack(int choice){
        setEstado(getEstado().applyBoilingWaterAttack(choice));
    }
    
    public void archersAttack(){
        setEstado(getEstado().archersAttack());
    }
    public void applyArchersAttack(int choice){
        setEstado(getEstado().applyArchersAttack(choice));
    }
    public void tunnelMovement(){
        setEstado(getEstado().tunnelMovement());
    }
    public void applyTunnelMovement(int choice,int move){
        setEstado(getEstado().applyTunnelMovement(choice, move));
    }
    public void endOfActionSelection(){
        setEstado(getEstado().endOfActionSelection());
    }
    public void resetJogo(){
        setEstado(getEstado().resetJogo());
    }
    
}
