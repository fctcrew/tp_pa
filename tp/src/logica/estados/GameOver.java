/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import java.util.Collections;
import logica.Datagame;

/**
 *
 * @author luis_
 */
public class GameOver extends EstadoAdapter{
    
    public GameOver(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado resetJogo(){
        
        getDatagame().getJogador().setCarriedSupplies(0);
        getDatagame().getJogador().setTunel(1);
        getDatagame().getJogador().setMorale(4);
        getDatagame().getJogador().setWallStrength(4);
        getDatagame().getJogador().setSupplies(4);
        
        getDatagame().getEnemy().setTrackCenter(4);
        getDatagame().getEnemy().setTrackLeft(4);
        getDatagame().getEnemy().setTrackRight(4);
        getDatagame().getEnemy().setTrebuchets(3);


        getDatagame().setDiaAtual(1);
        getDatagame().getBaralho().clear();
        getDatagame().GetRemovidasBaralho().clear();
        getDatagame().getBaralho().addAll(getDatagame().getCopiaReset());
        //getDatagame().getBaralho().addAll(getDatagame().GetRemovidasBaralho());
        Collections.shuffle(getDatagame().getBaralho());
        getDatagame().resetDrm();
        getDatagame().setGameover(0);
        getDatagame().setAddActionPointsUsed(false);
        getDatagame().setFreeMovementUsed(false);
        getDatagame().setBlockActions(false);
        getDatagame().setBoilingUsed(false);
        getDatagame().setSiegeTowerRemoved(false);
        getDatagame().clearMsgLog();
        return new AwaitBeginning(getDatagame());
    }
    
}
