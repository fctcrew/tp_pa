/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;
import logica.cards.EventCard;

/**
 *
 * @author joao
 */
public class AwaitTopCard extends EstadoAdapter{

    public AwaitTopCard(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado drawTopCard(){
        
        EventCard escolhida=this.getDatagame().getBaralho().remove(0); 
        getDatagame().GetRemovidasBaralho().add(escolhida);
       
        int dia=getDatagame().getDiaAtual();
        
        switch(dia){
            case 1:escolhida.Dia1();break;
            case 2:escolhida.Dia2();break;
            case 3:escolhida.Dia3();break;
        }
         getDatagame().clearMsgLog();
         getDatagame().addMsgLog(escolhida.toString());
        
        if(this.getDatagame().allCloseCombatArea() || getDatagame().isTwoOnZero()) { 
                return new GameOver(this.getDatagame());
            
        }
        if(this.getDatagame().isMandatory())
            this.getDatagame().CloseCombatArea();
        if(getDatagame().getGameover()==1)
            return new GameOver(getDatagame());
       
        return new AwaitActionSelection(this.getDatagame());        
    }
    
}
