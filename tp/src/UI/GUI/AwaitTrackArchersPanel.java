/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitTrackArchers;

/**
 *
 * @author luis_
 */
public class AwaitTrackArchersPanel extends JPanel implements Observer{
    private ObservableGame observableGame;
    
    private JButton trackLBtn,trackCBtn,trackRBtn;

    public AwaitTrackArchersPanel(ObservableGame observableGame) {
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        
        setOpaque(false);
        setVisible(observableGame.getEstado() instanceof AwaitTrackArchers);
        trackLBtn=new JButton("Ladder Track");
        trackCBtn=new JButton("Battering Ram Track");
        trackRBtn=new JButton("Siege Tower Track");
        setupComponents();
        add(trackLBtn);
        add(trackCBtn);
        add(trackRBtn);

        setupLayout();
        trackLBtn.addActionListener(new ApplyArchersAttackLeftListener());
        trackCBtn.addActionListener(new ApplyArchersAttackCenterListener());
        trackRBtn.addActionListener(new ApplyArchersAttackRightListener());
        update(observableGame,null);


    }
    
    
    private void setupComponents()
    { 
        trackLBtn.setPreferredSize(new Dimension(200,30));
        trackLBtn.setMinimumSize(new Dimension(200, 30));
        trackLBtn.setMaximumSize(new Dimension(200, 30));
        trackLBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        trackCBtn.setPreferredSize(new Dimension(200,30));
        trackCBtn.setMinimumSize(new Dimension(200, 30));
        trackCBtn.setMaximumSize(new Dimension(200, 30));
        trackCBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        trackRBtn.setPreferredSize(new Dimension(200,30));
        trackRBtn.setMinimumSize(new Dimension(200, 30));
        trackRBtn.setMaximumSize(new Dimension(200, 30));
        trackRBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        
    }
    
    private void setupLayout(){
       Box buttonsContainerCenter = Box.createVerticalBox();
       Box buttonsContainer = Box.createHorizontalBox();
  
        setLayout(new BorderLayout());
        
        buttonsContainer.add(Box.createHorizontalGlue());
        JLabel question =new JLabel("Choose the track that you want to attack: ");   
        question.setAlignmentX(Component.CENTER_ALIGNMENT);
        question.setFont(new Font("Arial", Font.BOLD, 18));
        buttonsContainerCenter.add(question);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(trackLBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(trackCBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));
        buttonsContainerCenter.add(trackRBtn);
        buttonsContainerCenter.add(Box.createRigidArea(new Dimension(10, 20)));

        buttonsContainer.add(buttonsContainerCenter);
        buttonsContainer.add(Box.createHorizontalGlue());
                
        
        //add(lbTitle, BorderLayout.NORTH);        
        add(buttonsContainer, BorderLayout.CENTER);
   }

    @Override
    public void update(Observable o, Object arg) {
       setVisible(observableGame.getEstado() instanceof AwaitTrackArchers);
    }
    
    class ApplyArchersAttackLeftListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyArchersAttack(1);
        }        
    }
    class ApplyArchersAttackCenterListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyArchersAttack(2);
        }        
    }
    class ApplyArchersAttackRightListener implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e){
            observableGame.applyArchersAttack(3);
        }        
    }
    
}
