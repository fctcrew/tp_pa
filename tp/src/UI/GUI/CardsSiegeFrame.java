/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import Files.FileUtility;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import logica.Jogo;
import logica.ObservableGame;
import logica.estados.AwaitBeginning;
import logica.estados.IEstado;

/**
 *
 * @author luis_
 */
public class CardsSiegeFrame extends JFrame implements Constants,Observer{
   
    private ObservableGame observableGame;       
    private CardsSiegePanel cardsiegePanel;
    private JMenuItem newObjJMI;
    
    public CardsSiegeFrame( ObservableGame j) {
        this( j, 200,100, 1600,960);
    }
    
    public CardsSiegeFrame( ObservableGame j, int x, int y ) {
        this( j, x,y, 1600, 960);
    }
    
    public CardsSiegeFrame( ObservableGame j, int x, int y, int width, int height) {
        
        super("9 Cards Siege"); 

        observableGame = j;
        this.observableGame.addObserver(this);
        
        Container cp = getContentPane(); 
            
        menus();
        cardsiegePanel= new CardsSiegePanel(observableGame);
        cp.add(cardsiegePanel, BorderLayout.CENTER);
        
        addComponents();
        
        setLocation(x, y); 
        setSize(width,height); 
        setMinimumSize(new Dimension(width,height));
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        validate();
        update(observableGame, null);
    
    }
    
    private void addComponents()
    {
        setLayout(new BorderLayout());        

        JLabel background = new JLabel(new ImageIcon(Constants.IMG_FUNDO));
        background.setLayout(new BorderLayout());
        
        background.add(cardsiegePanel);
        add(background, BorderLayout.CENTER);
    }
    
    private void menus() {        
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        // game menu
        JMenu gameMenu = new JMenu("Game");
        gameMenu.setMnemonic(KeyEvent.VK_G);
        
//        JMenuItem newObjJMI = new JMenuItem("Stop");
        newObjJMI = new JMenuItem("Stop");
        newObjJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newObjJMI.setMnemonic(KeyEvent.VK_S); 
        JMenuItem readObjJMI = new JMenuItem("Load");
        readObjJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
        
        JMenuItem saveObjJMI = new JMenuItem("Save");
        saveObjJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        
        JMenuItem exitJMI = new JMenuItem("Exit");
        exitJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));

        gameMenu.add(newObjJMI);
        gameMenu.add(readObjJMI);
        gameMenu.add(saveObjJMI);
        gameMenu.addSeparator();    
        gameMenu.add(exitJMI);
        menuBar.add(gameMenu);
                
        newObjJMI.addActionListener(new NewObjMenuBarListener());
        readObjJMI.addActionListener(new LoadObjMenuBarListener());
        saveObjJMI.addActionListener(new SaveObjMenuBarListener());
        exitJMI.addActionListener(new ExitListener());

        
        // help menu
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        
        JMenuItem helpContentJMI = new JMenuItem("Help Contents");
        helpContentJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));

        JMenuItem aboutJMI = new JMenuItem("About");
        aboutJMI.setAccelerator(
                KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
       
        helpMenu.add(helpContentJMI);
        helpMenu.add(aboutJMI);
        menuBar.add(helpMenu);
      
        helpContentJMI.addActionListener(new HelpContentListener());
        aboutJMI.addActionListener(new AboutListener());
        
    }
    class NewObjMenuBarListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // observableGame.finish();
        }
    }
    
    class LoadObjMenuBarListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser("./data");
            int returnVal = fc.showOpenDialog(CardsSiegeFrame.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();             
                try{
                    Jogo stateMachineGame = 
                            (Jogo)FileUtility.retrieveGameFromFile(file);
                    if(stateMachineGame != null){
                        observableGame.setJogo(stateMachineGame);
                    }
                }catch(IOException | ClassNotFoundException ex){
                    JOptionPane.showMessageDialog(CardsSiegeFrame.this, "Operation failed: \r\n\r\n" + e);
                }
          
            } else {
                System.out.println("Operation canceled ");
            }
        }
    }

    class SaveObjMenuBarListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser("./data");
            int returnVal = fc.showSaveDialog(CardsSiegeFrame.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                try{
                    FileUtility.saveGameToFile(file, observableGame.getJogo());
                }catch(IOException ex){
                    JOptionPane.showMessageDialog(CardsSiegeFrame.this, "Operation failed: \r\n\r\n" + e);
                }
            } else {
                System.out.println("Operation canceled ");
            }
        }
    }

    class ExitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    class HelpContentListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
//            JOptionPane.showMessageDialog(CardsSiegeFrame.this,"Take one ball from the bag ... ",
//                    "Help Contents", JOptionPane.PLAIN_MESSAGE);
                File f=new File("./src\\Files\\9CS_Rules_20171217.pdf");
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ex) {
                Logger.getLogger(CardsSiegeFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    class AboutListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(CardsSiegeFrame.this,
                    "Done by: Joao Nabais && Luis Pedroso\nDocente preferido: Maria Armanda <3",
                    "About", JOptionPane.PLAIN_MESSAGE);
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        IEstado state = observableGame.getEstado();
        newObjJMI.setEnabled(state instanceof AwaitBeginning );
     
       repaint();
    }
    
}
