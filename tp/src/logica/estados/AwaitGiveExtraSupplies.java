/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitGiveExtraSupplies extends EstadoAdapter{

    public AwaitGiveExtraSupplies(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado makeSpeech(boolean choice){
        int n=0;
    
        if(choice){
            this.getDatagame().getJogador().setSupplies(this.getDatagame().getJogador().getSupplies()-1);
            if(getDatagame().isTwoOnZero())
                return new GameOver(getDatagame());
            getDatagame().setDrmMorale(getDatagame().getDrmMorale()+1);
        }
            this.getDatagame().getDado().roll();
             n=this.getDatagame().getDado().getNumero();
             n+=getDatagame().getDrmMorale();         
             getDatagame().addMsgLog("DADO ja com DRM:"+n+"\n");
            if(n>4){
                this.getDatagame().getJogador().setMorale(this.getDatagame().getJogador().getMorale()+1);
            }
          getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()-1);  
        return new AwaitActionSelection(this.getDatagame());
    }
}
