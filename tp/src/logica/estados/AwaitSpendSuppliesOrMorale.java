/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitSpendSuppliesOrMorale extends EstadoAdapter{

    public AwaitSpendSuppliesOrMorale(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado spendSuppliesOrMorale(int choice){
        if(choice==1){
            getDatagame().getJogador().setMorale(this.getDatagame().getJogador().getMorale()-1);
        }else{
            getDatagame().getJogador().setSupplies(this.getDatagame().getJogador().getSupplies()-1);

        }
        if(getDatagame().isTwoOnZero())
            return new GameOver(getDatagame());
        getDatagame().setActionPointsAtual(getDatagame().getActionPointsAtual()+1);
        getDatagame().setAddActionPointsUsed(true);
        return new AwaitActionSelection(this.getDatagame());
    }
    
}
