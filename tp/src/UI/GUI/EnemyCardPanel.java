/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import static UI.GUI.Constants.IMG_CARD2;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitBeginning;

/**
 *
 * @author luis_
 */
public class EnemyCardPanel extends JPanel implements Constants,Observer{
    
    private ObservableGame observableGame;
    private JLabel image;
    
    private List<Rectangle> squares = new ArrayList<Rectangle>();
    private Rectangle trebuchet;
    private Rectangle closeCombat;
    private int tokenSizeX = 56, tokenSizeY = 58;
    private int subir=73;
    
    public EnemyCardPanel(ObservableGame observableGame){
        this.observableGame = observableGame;
        this.observableGame.addObserver(this);
        trebuchet=new Rectangle(345, 301+subir+15, tokenSizeX, tokenSizeY);
        closeCombat=new Rectangle(180+tokenSizeX-17,14-2,(2*tokenSizeX)-5,tokenSizeY);
        setOpaque(false);
        setupComponents();
        add(image);
    }
    
    private void setupComponents()
    { 
        image = new JLabel();
        image.setIcon(new ImageIcon(Common.getScaledImg(IMG_ENEMY, 300, 450)));
        squares.add(new Rectangle(141, 301, tokenSizeX, tokenSizeY)); // leftTrack
        squares.add(new Rectangle(243, 301, tokenSizeX, tokenSizeY)); // CenterTrack
        squares.add(new Rectangle(345, 301, tokenSizeX, tokenSizeY)); // RightTrack REAL
    }
    
    private void atualizaLeft()
    {
        int x=141,y=301;
        if(observableGame.getDataGame().getEnemy().getTrackLeft()==3)
            y-=subir;
        if(observableGame.getDataGame().getEnemy().getTrackLeft()==2)
            y=y-subir-subir;
        squares.get(0).setLocation(x, y);
    }
    private void atualizaCenter()
    {
        int x=243,y=301;
        if(observableGame.getDataGame().getEnemy().getTrackCenter()==3)
            y-=subir;
        if(observableGame.getDataGame().getEnemy().getTrackCenter()==2)
            y=y-subir-subir;
        squares.get(1).setLocation(x, y);
    }
    private void atualizaRight()
    {
        int x=345,y=301;
        if(observableGame.getDataGame().getEnemy().getTrackRight()==3)
            y-=subir;
        if(observableGame.getDataGame().getEnemy().getTrackRight()==2)
            y=y-subir-subir;
        squares.get(2).setLocation(x, y);
    }
    private void atualizaT(){
        int x=345,y=316+subir;
        if(observableGame.getDataGame().getEnemy().getTrebuchets()==2)
            x=x-28-subir;
        if(observableGame.getDataGame().getEnemy().getTrebuchets()==1)
            x=x-subir-subir-tokenSizeX-1;
        trebuchet.setLocation(x,y);
    }
    
    
    protected void paintChildren(Graphics g) 
    {
        int i = 0;
        
        super.paintChildren(g);
        
        Graphics2D g2 = (Graphics2D) g;
        
        final float dash1[] = {0.1f};
        final BasicStroke dashed =  new BasicStroke(3f,
                                                    BasicStroke.CAP_SQUARE,
                                                    BasicStroke.JOIN_ROUND,
                                                    0.2f, dash1, 0.0f);
        
        
        for (Rectangle rect : squares) {
            
            g2.setColor(new Color(1, 1, 1, 0.15f));
            g2.fill(rect);
            g2.setStroke(dashed);
            g2.setColor(Color.BLUE);////
            if(observableGame.getDataGame().getEnemy().getTrackLeft()>1 && i==0)
                g2.draw(rect);
            if(observableGame.getDataGame().getEnemy().getTrackCenter()>1 && i==1)
                g2.draw(rect);
            if(observableGame.getDataGame().getEnemy().getTrackRight()>1 && i==2 && !observableGame.getDataGame().isSiegeTowerRemoved())
                g2.draw(rect);
            i++;
            
        }
        if(!observableGame.getDataGame().NoOneClosecombatArea())
            g2.draw(closeCombat);
        
        g2.setColor(Color.YELLOW);
        if(observableGame.getDataGame().getEnemy().getTrebuchets()>0)
            g2.draw(trebuchet);
       
        g2.setColor(Color.BLUE);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(3));
        if(observableGame.getDataGame().getEnemy().getTrackLeft()==1)
            g2.drawOval(139, 301-subir-subir-subir-4, 62, 63);
        if(observableGame.getDataGame().getEnemy().getTrackCenter()==1)
            g2.drawOval(241, 301-subir-subir-subir-4, 62, 63);
        if(observableGame.getDataGame().getEnemy().getTrackRight()==1)
            g2.drawOval(343, 301-subir-subir-subir-4, 62, 63);
        
        
    }
    
    @Override
    public void update(Observable o, Object arg) {
//       Rectangle rect = squares.get(0); 
//       float x = 427, y = 625;
//       rect.setLocation((int)x, (int)y);
//       revalidate();
        if(!(observableGame.getEstado() instanceof AwaitBeginning))
        {
            atualizaLeft();
            atualizaCenter();
            atualizaRight();
            atualizaT();
            revalidate();
        }
    }
}
