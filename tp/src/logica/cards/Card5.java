/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;



/**
 *
 * @author joao
 */
public class Card5 extends EventCard{
    
        public Card5(Datagame g) {
        super(g);
    }
    
      @Override
    public void Dia1(){
        this.getDatagame().setActionPointsAtual(3);
        this.getDatagame().setDrmLadder(1);
        this.getDatagame().setDrmSiegeTower(1);
        this.getDatagame().setDrmBatteringRam(1);
        this.getDatagame().setDrmCloseCombat(1);
        
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        
        this.setEventName("Volley of Arrows");
    }
    
    @Override
    public void Dia2(){
        this.setEventName("Collapsed");
        this.getDatagame().setActionPointsAtual(2);
   
        if(this.getDatagame().getEnemy().getTrackRight()==4)
            this.getDatagame().setSiegeTowerRemoved(true);
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
             if(getDatagame().getEnemy().getTrackLeft()==0){
                getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
                if(getDatagame().isTwoOnZero())
                    return;
             }
        }
        if(getDatagame().getEnemy().getTrackCenter()>0){
            this.getDatagame().getEnemy().setTrackCenter(this.getDatagame().getEnemy().getTrackCenter()-1);
             if(getDatagame().getEnemy().getTrackCenter()==0)
                    getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        
        
    }
    
    @Override
    public void Dia3(){
        this.getDatagame().setActionPointsAtual(2);
        this.getDatagame().setDrmCoupure(1);
        
        if(this.getDatagame().getEnemy().getTrebuchets()<3)
            this.getDatagame().getEnemy().setTrebuchets(getDatagame().getEnemy().getTrebuchets()+1);
        
        if(getDatagame().getEnemy().getTrackLeft()>0){
            this.getDatagame().getEnemy().setTrackLeft(this.getDatagame().getEnemy().getTrackLeft()-1);
             if(getDatagame().getEnemy().getTrackLeft()==0)
                    getDatagame().getJogador().setMorale(getDatagame().getJogador().getMorale()-1);
        }
        this.setEventName("Repaired Trebuchet");

    }
}
