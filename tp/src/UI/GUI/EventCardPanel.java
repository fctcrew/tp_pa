/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitActionSelection;

/**
 *
 * @author joao
 */
public class EventCardPanel extends JPanel implements Constants, Observer{

    private ObservableGame observableGame;
    private JLabel imagemCarta;
    
    public EventCardPanel(ObservableGame g)
    {
        this.observableGame = g;
        
        this.observableGame.addObserver(this);
        
        setVisible(observableGame.getEstado()instanceof AwaitActionSelection);
        
        setOpaque(false);
        
        imagemCarta=new JLabel();
        setupComponents();
        add(imagemCarta);
        
//        updateCards();
//        selectActualColumn();
    }
    private void setupComponents()
    { 
        
        imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CAPA, 200, 450)));
        
    }
    private void updateCard()
    {
        String nome;
        nome=observableGame.getDataGame().getRemovidas().get(observableGame.getDataGame().getRemovidas().size()-1).getEventName();
        if(nome.equalsIgnoreCase("Ilness") || nome.equalsIgnoreCase("Guards Distracted") || nome.equalsIgnoreCase("Trebuchet Attack!"))
            imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD2, 250, 350)));
        else
            if(nome.equalsIgnoreCase("Supplies Spoiled") || nome.equalsIgnoreCase("Bad Weather") || nome.equalsIgnoreCase("Boiling Oil"))
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD3, 250, 350)));
        else    
            if(nome.equalsIgnoreCase("Death OF Leader") || nome.equalsIgnoreCase("gate fortified") || nome.equalsIgnoreCase("flaming arrows"))
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD4, 250, 350)));
        else    
            if(nome.equalsIgnoreCase("volley of arrows") || nome.equalsIgnoreCase("collapsed") || nome.equalsIgnoreCase("repaired trebuchet"))
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD5, 250, 350)));
        else    
            if(nome.equalsIgnoreCase("cover of darkness") || nome.equalsIgnoreCase("enemy fatigue") || nome.equalsIgnoreCase("rally"))
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD6, 250, 350)));
        else    
            if(nome.equalsIgnoreCase("determined enemy") || nome.equalsIgnoreCase("iron shields") || nome.equalsIgnoreCase("faith"))
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD7, 250, 350)));
        else
                imagemCarta.setIcon(new ImageIcon(Common.getScaledImg(IMG_CARD1, 250, 350)));
        
    }
    @Override
    public void update(Observable o, Object o1) {
        setVisible(observableGame.getEstado()instanceof AwaitActionSelection);
        if(observableGame.getEstado()instanceof AwaitActionSelection)
            updateCard();
    }
    
}
