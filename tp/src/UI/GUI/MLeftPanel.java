/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Box;
import javax.swing.JPanel;
import logica.ObservableGame;
import logica.estados.AwaitBeginning;

/**
 *
 * @author joao
 */
public class MLeftPanel extends JPanel implements Constants, Observer{
    private ObservableGame observableGame;
    private LogPanel logPanel;
    private EnemyCardPanel enemyCardPanel;
    
    public MLeftPanel(ObservableGame g)
    {
        observableGame = g;
        
        observableGame.addObserver(this);
        
        setOpaque(false);
        
        setupComponents();
        setupLayout();
    }

     private void setupComponents()
    {       
        logPanel = new LogPanel(observableGame);
        enemyCardPanel = new EnemyCardPanel(observableGame);
        
        enemyCardPanel.setVisible(!(observableGame.getEstado() instanceof AwaitBeginning));
    }

    private void setupLayout()
    {
        setLayout(new BorderLayout());
        
        Box container = Box.createVerticalBox();
        
        
        container.add(Box.createRigidArea(new Dimension(550, 100)));
        container.add(enemyCardPanel);
        
        add(container, BorderLayout.NORTH);
        
        //add(logPanel, BorderLayout.SOUTH);
        
        add(container);
    }
    
    @Override
    public void update(Observable o, Object o1) {
        enemyCardPanel.setVisible(!(observableGame.getEstado() instanceof AwaitBeginning));
    }
}
