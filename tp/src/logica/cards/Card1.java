/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.cards;

import logica.Datagame;


/**
 *
 * @author joao
 */
public class Card1 extends EventCard{

    public Card1(Datagame g) {
        super(g);
    }
    
    
    @Override
    public void Dia1(){
        getDatagame().setActionPointsAtual(3);
        if(this.getDatagame().getEnemy().getTrebuchets()==3)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-2);
        
        if(this.getDatagame().getEnemy().getTrebuchets()==2)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        
        if(this.getDatagame().getEnemy().getTrebuchets()==1)
        {
           getDatagame().getDado().roll();
           getDatagame().addMsgLog("Trebuchet 1 foi preciso rolar dado \nDado:"+getDatagame().getDado().getNumero()+"\n");
           if(getDatagame().getDado().getNumero()>3)
               this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        }
        this.setEventName("Trebuchet Attack");
        
    }
    
    @Override
    public void Dia2(){
        this.getDatagame().setActionPointsAtual(2);
        if(this.getDatagame().getEnemy().getTrebuchets()==3)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-2);
        if(this.getDatagame().getEnemy().getTrebuchets()==2)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        if(this.getDatagame().getEnemy().getTrebuchets()==1)
        {
           getDatagame().getDado().roll();
           getDatagame().addMsgLog("Trebuchet 1 foi preciso rolar dado \nDado:"+getDatagame().getDado().getNumero()+"\n");
           if(getDatagame().getDado().getNumero()>3)
               this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        }
        this.setEventName("Trebuchet Attack");

    }
    
    @Override
    public void Dia3(){
        this.getDatagame().setActionPointsAtual(1);
        if(this.getDatagame().getEnemy().getTrebuchets()==3)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-2);
        if(this.getDatagame().getEnemy().getTrebuchets()==2)
            this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        if(this.getDatagame().getEnemy().getTrebuchets()==1)
        {
           getDatagame().getDado().roll();
           getDatagame().addMsgLog("Trebuchet 1 foi preciso rolar dado \nDado:"+getDatagame().getDado().getNumero()+"\n");
           if(getDatagame().getDado().getNumero()>3)
               this.getDatagame().getJogador().setWallStrength(getDatagame().getJogador().getWallStrength()-1);
        }
        this.setEventName("Trebuchet Attack");

    }

}
