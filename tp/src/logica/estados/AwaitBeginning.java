/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica.estados;

import logica.Datagame;

/**
 *
 * @author joao
 */
public class AwaitBeginning extends EstadoAdapter{

    public AwaitBeginning(Datagame data) {
        super(data);
    }
    
    @Override
    public IEstado comecarJogo(){
        return new AwaitTopCard(this.getDatagame());
    }
}
